var a00184 =
[
    [ "~WorkerPool", "d2/d94/a00184.html#a9e3f8f20a5dbfb44b07cb195911dcc80", null ],
    [ "Delete", "d2/d94/a00184.html#ad275a451a5f56d10685e97e810fa5cdf", null ],
    [ "getProcess", "d2/d94/a00184.html#a6b3810abccd98fac413f98532d5ed415", null ],
    [ "globalInstance", "d2/d94/a00184.html#a436b452b56b47eaffc9d59ddfa70cedc", null ],
    [ "MakeProcessAvailable", "d2/d94/a00184.html#ad26d4bd4da3b5f8479df0789017039a1", null ],
    [ "MakeProcessAvailable", "d2/d94/a00184.html#a7ef887dae2e5fe261e0e8f4d36fb988b", null ],
    [ "NewWorkerAvailable", "d2/d94/a00184.html#a69ba4d5e02c18fc591f5e564e5f378ab", null ],
    [ "ReleaseWorker", "d2/d94/a00184.html#a0958e94693a9954db7b5e26d1c83bc3a", null ],
    [ "ReleaseWorker", "d2/d94/a00184.html#ac9f9a719b4fe3dca8ec0112bf495776a", null ],
    [ "SetMinNumWorkers", "d2/d94/a00184.html#a2b82235c48826c7574f9f344e4650828", null ],
    [ "WorkerAvailable", "d2/d94/a00184.html#ad99e94826598f396769d283205cfcc45", null ],
    [ "WorkerReconnected", "d2/d94/a00184.html#a3872b1e2c9e23573965e19352464cd04", null ]
];