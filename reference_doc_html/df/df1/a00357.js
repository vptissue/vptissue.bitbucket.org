var a00357 =
[
    [ "ChildrenType", "df/df1/a00357.html#aef3ce76fd718dad52c5591ed71da2d00", null ],
    [ "HasUnsavedChanges", "df/df1/a00357.html#ac7e646a3ccc5a62a7acc0055bf1ab1d7", null ],
    [ "~HasUnsavedChanges", "df/df1/a00357.html#a43402e3ab8fa628a8b8782beaef9c27e", null ],
    [ "AddChild", "df/df1/a00357.html#a21fe99ef1aaf826b4221060a17918ca7", null ],
    [ "begin", "df/df1/a00357.html#aa5fc722335ba28d39b51ee4b7ba4c70e", null ],
    [ "begin", "df/df1/a00357.html#a9643dd5e4e172a4c351f450e917d9a4a", null ],
    [ "end", "df/df1/a00357.html#a4125b0bf4af9a0d3302ee99b1db14aef", null ],
    [ "end", "df/df1/a00357.html#adbafd59854ef6a577145a41fe2275d24", null ],
    [ "ForceClose", "df/df1/a00357.html#a091e25e1e741f736d60dc5eef1e852fd", null ],
    [ "GetTitle", "df/df1/a00357.html#a5ff089eb3596bf2508b58207ce29cb5c", null ],
    [ "InternalForceClose", "df/df1/a00357.html#a15aa2cddf7d6e4f2022d6f3eadd4af8f", null ],
    [ "InternalIsClean", "df/df1/a00357.html#a29656e7cc079bc7f1c3ed0e41d8a37a4", null ],
    [ "InternalPreForceClose", "df/df1/a00357.html#a7a16b8e588bd73533b8997bfef96ac5e", null ],
    [ "InternalSave", "df/df1/a00357.html#a3bd25fa5852609521727c29d2a88b30d", null ],
    [ "IsClean", "df/df1/a00357.html#a4dc04cbaee0e4ace196590bce67797ab", null ],
    [ "IsOpened", "df/df1/a00357.html#a05077ae76169d863e9659cdb4c6adbd6", null ],
    [ "Save", "df/df1/a00357.html#ae4ff7b7846ac1c4476e3427dc9ed149f", null ],
    [ "SaveAndClose", "df/df1/a00357.html#aa9ab1dc0fbba348664bdce78f419bb1f", null ],
    [ "SetChildren", "df/df1/a00357.html#a93e34d1291d1742404982a19c5969a39", null ]
];