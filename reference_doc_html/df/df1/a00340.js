var a00340 =
[
    [ "TransportEquations", "df/df1/a00340.html#acb65e3d73916c2da744106d0fe2539e0", null ],
    [ "GetDerivatives", "df/df1/a00340.html#a109d8db2fe497718079d232dc0601226", null ],
    [ "GetEquationCount", "df/df1/a00340.html#ac423ea4274572e728ff596b348c60df8", null ],
    [ "GetVariables", "df/df1/a00340.html#a2c0bb7cb79dd7372f9299b96b87127e6", null ],
    [ "Initialize", "df/df1/a00340.html#a336248aefc375cec9db4c7dee2fa44d3", null ],
    [ "SetVariables", "df/df1/a00340.html#a84815297b41a5c97c69e57ba0f71ed16", null ]
];