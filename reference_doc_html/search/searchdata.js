var indexSectionsWithContent =
{
  0: "abcdefghiklmnopqrstuvwx~",
  1: "abcdefghilmnopqrstuvwx",
  2: "msu",
  3: "abcdefghilmnopqrstuvwx",
  4: "abcdefghiklmnoprstuvw~",
  5: "cgmnpru",
  6: "cdfhimstw",
  7: "bimst",
  8: "cfr",
  9: "t",
  10: "v"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "related",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Friends",
  10: "Pages"
};

