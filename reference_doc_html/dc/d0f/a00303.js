var a00303 =
[
    [ "GeoData", "dc/d0f/a00303.html#a95ffad9eab2effa68053f2c2929e6b67", null ],
    [ "GeoData", "dc/d0f/a00303.html#ad8846447215c07be6fb7de054f930071", null ],
    [ "GetEllipseAxes", "dc/d0f/a00303.html#a00dd0d1b208966e536f8094cba5f24aa", null ],
    [ "operator+=", "dc/d0f/a00303.html#aee1e51128abbdcf372a0972cd54fa954", null ],
    [ "operator-=", "dc/d0f/a00303.html#a080f007b8a48039982e3e43da9d07d23", null ],
    [ "m_area", "dc/d0f/a00303.html#a5299bf603b25d044c620feb2916e5d56", null ],
    [ "m_area_moment", "dc/d0f/a00303.html#adf9c2b8f817d55a981c203d99c40794a", null ],
    [ "m_centroid", "dc/d0f/a00303.html#abf30953ff61b2b1d989e329d48ccaa3a", null ]
];