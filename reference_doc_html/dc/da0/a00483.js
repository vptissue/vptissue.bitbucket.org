var a00483 =
[
    [ "BoundaryType", "dc/da0/a00483.html#adc71f9d51ca389aaaf3e3f5ba5fc1af6", [
      [ "None", "dc/da0/a00483.html#adc71f9d51ca389aaaf3e3f5ba5fc1af6a6adf97f83acf6453d4a6a4b1070f3754", null ],
      [ "NoFlux", "dc/da0/a00483.html#adc71f9d51ca389aaaf3e3f5ba5fc1af6aa5af8c34dc3d5f295a4ce78e0930690d", null ],
      [ "SourceSink", "dc/da0/a00483.html#adc71f9d51ca389aaaf3e3f5ba5fc1af6a6919f0fe04176bccb463fecf7bb3a052", null ],
      [ "SAM", "dc/da0/a00483.html#adc71f9d51ca389aaaf3e3f5ba5fc1af6a9f1b3be4a82b11d104e4ef7f7ccb1c19", null ]
    ] ],
    [ "operator<<", "dc/da0/a00483.html#a57334186b396a05efb36a6db4431cd9a", null ],
    [ "operator>>", "dc/da0/a00483.html#a9cddd7985a659e8fd48b3eab52e0cf37", null ]
];