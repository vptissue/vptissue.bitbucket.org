var a00194 =
[
    [ "EntryType", "d3/d2a/a00195.html", "d3/d2a/a00195" ],
    [ "ConversionList", "dc/d57/a00194.html#aa81146829abe345e09865235275e5cf3", null ],
    [ "~ConversionList", "dc/d57/a00194.html#a2fa7848e56c0d369cd723655af27aeb1", null ],
    [ "AddEntry", "dc/d57/a00194.html#acecf10cbc44db62446f8fcb6d0420a5e", null ],
    [ "CheckedChanged", "dc/d57/a00194.html#aefa0843d341c7ae58842003c8392d4ed", null ],
    [ "Clear", "dc/d57/a00194.html#ad16c436e55e3b30dfe85421cf090cd9c", null ],
    [ "GetCheckedEntries", "dc/d57/a00194.html#a069f7d9e41d13666ae2611c3747a8bf9", null ],
    [ "HasCheckedEntries", "dc/d57/a00194.html#adacf8a62654413edf32aabb0e0ae488b", null ],
    [ "SelectionChanged", "dc/d57/a00194.html#a65a8ba147ab36beb15b179e4a9d7d54b", null ]
];