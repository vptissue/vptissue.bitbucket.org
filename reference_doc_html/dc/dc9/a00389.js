var a00389 =
[
    [ "WorkspaceView", "dc/dc9/a00389.html#ab9e5f841780ffd46a4b2f82586560990", null ],
    [ "~WorkspaceView", "dc/dc9/a00389.html#a3eb4ce33d0bef2ac8df857a9597b791b", null ],
    [ "contextMenuEvent", "dc/dc9/a00389.html#a3c8cfe958cd35a1c256f89a2be012de8", null ],
    [ "GetOpenAction", "dc/dc9/a00389.html#a1639bb10e180bf30298d40598f79aecf", null ],
    [ "GetRemoveAction", "dc/dc9/a00389.html#a197573bf37e3b91695c2a5a7a70c5fd7", null ],
    [ "GetRenameAction", "dc/dc9/a00389.html#a43f6e57dea5283f63e606ba769effc6b", null ],
    [ "mouseDoubleClickEvent", "dc/dc9/a00389.html#a36758da10bbde028f8df0ae111126ba0", null ],
    [ "ProjectRemoved", "dc/dc9/a00389.html#abf7a291c0e92f6661063f65677f5fc4b", null ],
    [ "ProjectRenamed", "dc/dc9/a00389.html#ae0fad4e8e3e2a744fcff83f809b7030c", null ],
    [ "setModel", "dc/dc9/a00389.html#a8b4b7bdc9e235608a7ba783d0620b717", null ]
];