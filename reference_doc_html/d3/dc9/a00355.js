var a00355 =
[
    [ "WorkspaceController", "d3/dc9/a00355.html#a5ae80f34ef05275a36416e2cbf31d388", null ],
    [ "GetActionNewProject", "d3/dc9/a00355.html#a5d6fd0074a9b1b0cf4d58a51cff90653", null ],
    [ "GetActionRefreshWorkspace", "d3/dc9/a00355.html#af9e332eec2a4ac389bf3b78bd0e908fc", null ],
    [ "GetPreferencesDock", "d3/dc9/a00355.html#a8297c05dec376532d0252f7033b47ba2", null ],
    [ "GetPreferencesMenu", "d3/dc9/a00355.html#ad259352c171196962952a530700d141a", null ],
    [ "GetPTreeState", "d3/dc9/a00355.html#a54e4435664c600ebd1c0dd091bc7c5bb", null ],
    [ "GetView", "d3/dc9/a00355.html#aba109947e98fe5f65653099db65bc547", null ],
    [ "InternalForceClose", "d3/dc9/a00355.html#a9adbc676de44d1116959406cc93332be", null ],
    [ "InternalIsClean", "d3/dc9/a00355.html#aa04b576a64912e44624e0e6ad39f6e7d", null ],
    [ "InternalPreForceClose", "d3/dc9/a00355.html#a44aa108765ce87689844628e19405b7c", null ],
    [ "InternalSave", "d3/dc9/a00355.html#a68acc89427f4da2f33069936898ea578", null ],
    [ "IsOpened", "d3/dc9/a00355.html#a22a0843004fe261b68e018d0bc054a74", null ],
    [ "Open", "d3/dc9/a00355.html#a240d1e3548fc8ce58d4368c54aff0c8f", null ],
    [ "operator bool", "d3/dc9/a00355.html#a4680628cb9e54747ca901c918b517492", null ],
    [ "operator*", "d3/dc9/a00355.html#a18067cfadc1f3009cc34b6d5ac6ebfdc", null ],
    [ "operator*", "d3/dc9/a00355.html#ae4a054db5bdbf853115f97f4df637b3b", null ],
    [ "operator->", "d3/dc9/a00355.html#aea62c8f785c0266641ea20a02102877e", null ],
    [ "operator->", "d3/dc9/a00355.html#acab8496fc675264060a3570599e8541f", null ],
    [ "Project", "d3/dc9/a00355.html#aa5168c96cfeea66c0d2a434c876de321", null ],
    [ "Project", "d3/dc9/a00355.html#a2b0ec075f7486e1431b62d50f55f142c", null ],
    [ "SetPTreeState", "d3/dc9/a00355.html#a239e26004ba0aa0b68dd67874963476f", null ]
];