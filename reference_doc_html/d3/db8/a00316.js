var a00316 =
[
    [ "NodeAttributes", "d3/db8/a00316.html#ab272d02c64fcb2859983d70d92b7e4e5", null ],
    [ "~NodeAttributes", "d3/db8/a00316.html#a426a14c3358502f33d9c6d0320e00a96", null ],
    [ "IsFixed", "d3/db8/a00316.html#ad8e834e92f9c2a9738cfac286b262f18", null ],
    [ "IsSam", "d3/db8/a00316.html#a0e6bede385c37791d9b44becabec6c13", null ],
    [ "ReadPtree", "d3/db8/a00316.html#a765ec591bca2d89e1b9b028fa72878a1", null ],
    [ "SetFixed", "d3/db8/a00316.html#afdc4510abe9bf706fba4f801e539da1d", null ],
    [ "SetSam", "d3/db8/a00316.html#a99de69fed15b027b77d7e019cb5f46c6", null ],
    [ "ToPtree", "d3/db8/a00316.html#a91ab9b677e830b5dbf423a3e76389fe0", null ]
];