var a00185 =
[
    [ "WorkerRepresentative", "de/df3/a00185.html#a101ab813114fbd03b70c7cb1780fea17", null ],
    [ "WorkerRepresentative", "de/df3/a00185.html#a98488596f0d4ab7e28580ec73f72a9b6", null ],
    [ "~WorkerRepresentative", "de/df3/a00185.html#a4cca560ef204c12a18d1efb8a02dfb82", null ],
    [ "Delete", "de/df3/a00185.html#af3af98925d3fd57a7159ed7ae15b4eff", null ],
    [ "Disconnected", "de/df3/a00185.html#a622c9fa1a088e30d82a85a313bb7f9c5", null ],
    [ "DoWork", "de/df3/a00185.html#ade3bd76666573962625b85621a823ccb", null ],
    [ "FinishedWork", "de/df3/a00185.html#a3b6c3af6ace7edda0d3a1d5cbfd18170", null ],
    [ "GetExplName", "de/df3/a00185.html#a8ea65bc2a9bc4928f3eb25a192f1df64", null ],
    [ "GetSenderAddress", "de/df3/a00185.html#a61432146183887dd9e8fffa1bfddf068", null ],
    [ "GetSenderPort", "de/df3/a00185.html#a0b74a9273cb1d39e93d173fab2e81263", null ],
    [ "GetTaskId", "de/df3/a00185.html#a63310e987e83346d2962c6e976c57a1e", null ],
    [ "ReadyToWork", "de/df3/a00185.html#a99599d0c93ef1e10c1da6b919419c0ad", null ],
    [ "Setup", "de/df3/a00185.html#a9b4dfc398c9be484459d97a2211b3c9f", null ],
    [ "StopTask", "de/df3/a00185.html#a3d912b5dc341ac5950c9f95c0041d686", null ]
];