var a00146 =
[
    [ "UndoStack", "d9/db6/a00146.html#ac07071c01e0c44dd8cf09dcc28f8bffc", null ],
    [ "~UndoStack", "d9/db6/a00146.html#ae2ab146ae72a9e9730bf8095222da985", null ],
    [ "CanRedo", "d9/db6/a00146.html#a82ebb2c9fc386222bce62fba011218ab", null ],
    [ "CanUndo", "d9/db6/a00146.html#aef8c20c46473bc87df98ae132d1f9d92", null ],
    [ "Initialize", "d9/db6/a00146.html#aced251291b49c77e5b333b3bcd38872f", null ],
    [ "Push", "d9/db6/a00146.html#a305011f1432a9cacad376e4a0a088470", null ],
    [ "Redo", "d9/db6/a00146.html#ab83f69694e7403fcf788233440f6c90f", null ],
    [ "Undo", "d9/db6/a00146.html#a7a3c16f7014171bdb24be5168d64b23f", null ]
];