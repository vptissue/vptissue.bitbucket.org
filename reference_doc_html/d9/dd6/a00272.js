var a00272 =
[
    [ "TClock", "d9/dd6/a00272.html#a7a96af1ae51f84a118b00560fbd8a95d", null ],
    [ "Stopwatch", "d9/dd6/a00272.html#a68b726decd34aba5b633a3e4aaeaff4b", null ],
    [ "Get", "d9/dd6/a00272.html#a69992c7a0561164cacaa3243542e6ff6", null ],
    [ "GetName", "d9/dd6/a00272.html#a70e06b9765e5293a9a30f8d79359f3f7", null ],
    [ "IsRunning", "d9/dd6/a00272.html#a5d1ca44f686b8e87308bed260d4ca43a", null ],
    [ "Reset", "d9/dd6/a00272.html#a481d05cb86a6b7bc4759ef50446c9016", null ],
    [ "Start", "d9/dd6/a00272.html#aa51d0208c3b9b2f9ab6a2244eb79a4e8", null ],
    [ "Stop", "d9/dd6/a00272.html#a281514b0a64745e88076b5e535240834", null ],
    [ "ToString", "d9/dd6/a00272.html#a4be335c697a8cc7fb5817a3c87507dbf", null ]
];