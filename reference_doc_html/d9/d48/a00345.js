var a00345 =
[
    [ "CallbackType", "d9/d48/a00345.html#a86fb7847e29eb26d68721dfcdb8d8088", null ],
    [ "EventType", "d9/d48/a00345.html#a53f17b6fc4a1ce45c960bbd7c735cd03", null ],
    [ "KeyType", "d9/d48/a00345.html#a7ee6736abcb3bf1adb3f753100847633", null ],
    [ "~Subject", "d9/d48/a00345.html#a3e60de7079820a035a70f29fecde2285", null ],
    [ "Notify", "d9/d48/a00345.html#a37fd2d48abea374c795a09372e1122c5", null ],
    [ "Register", "d9/d48/a00345.html#a8d85ec0e0402179309a0f7d9d9093127", null ],
    [ "Unregister", "d9/d48/a00345.html#a69f72d4e5c2f64461532fa9c2788a457", null ],
    [ "UnregisterAll", "d9/d48/a00345.html#a081778d29015ce1a7723a82553e0a643", null ]
];