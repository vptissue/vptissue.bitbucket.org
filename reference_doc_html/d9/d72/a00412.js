var a00412 =
[
    [ "Type", "d9/d72/a00412.html#a5048d3f00711c728d5d56551ebcfaa76", [
      [ "ProjectAdded", "d9/d72/a00412.html#a5048d3f00711c728d5d56551ebcfaa76a921b9abe3b1ef020a0db70cabd86cfee", null ],
      [ "ProjectRenamed", "d9/d72/a00412.html#a5048d3f00711c728d5d56551ebcfaa76a3431a29c2185b0f505a6d86d23ea1ddf", null ],
      [ "ProjectRemoved", "d9/d72/a00412.html#a5048d3f00711c728d5d56551ebcfaa76a1f22b2fe472d3c4f96ff605ac41dfa4b", null ]
    ] ],
    [ "WorkspaceChanged", "d9/d72/a00412.html#a0808158324e3ad6e8581fb3c2241dcae", null ],
    [ "WorkspaceChanged", "d9/d72/a00412.html#a1ec3634d1d805827b6fbb1b94dc2409c", null ],
    [ "~WorkspaceChanged", "d9/d72/a00412.html#ac485725ae05178e6cc4b73de71cb9f94", null ],
    [ "GetName", "d9/d72/a00412.html#a985dfc72074f53735a5c1aa8d754dbee", null ],
    [ "GetNewName", "d9/d72/a00412.html#a17ed00df8925d0e1b2d920c572938919", null ],
    [ "GetOldName", "d9/d72/a00412.html#a31551500a53649247381045611ab6080", null ],
    [ "GetType", "d9/d72/a00412.html#a13b80fb501b4b033df37f26d2632f9d6", null ]
];