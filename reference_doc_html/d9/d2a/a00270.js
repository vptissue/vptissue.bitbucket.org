var a00270 =
[
    [ "Duration", "d9/d2a/a00270.html#a10b9b193e41e6b189da77b78932d4639", null ],
    [ "Clear", "d9/d2a/a00270.html#a687cd4056e0b8a198cea5ef068dfc3a7", null ],
    [ "GetCount", "d9/d2a/a00270.html#aa1a5dd83cc83073dad920ab8450614a3", null ],
    [ "GetCumulative", "d9/d2a/a00270.html#a11da0b31fef4a4d80267ef19e6fb80c9", null ],
    [ "GetMean", "d9/d2a/a00270.html#a99d59ecf1b19a911ba489bc0af0edd18", null ],
    [ "GetNames", "d9/d2a/a00270.html#a1754c3794050fb979c0f653e3cb6f752", null ],
    [ "GetRecords", "d9/d2a/a00270.html#a461bb23de18f0ace9fe389a92df32fe1", null ],
    [ "GetRecords", "d9/d2a/a00270.html#ad1a624e6aea09395b1a176eedb377d86", null ],
    [ "IsPresent", "d9/d2a/a00270.html#a9378e92db4769c40dfed4a07ff4c09b6", null ],
    [ "Merge", "d9/d2a/a00270.html#ac81990b189c5321a8294c22b6fd41d95", null ],
    [ "Merge", "d9/d2a/a00270.html#a155913a3c523e76158cfeb5067968420", null ],
    [ "Record", "d9/d2a/a00270.html#a66db2af103e0f40f863bc537a1fc8d34", null ],
    [ "Record", "d9/d2a/a00270.html#a7c9d79a05cef8c14a8dcfb5636f991d8", null ]
];