var a01139 =
[
    [ "Auxin", "d2/db1/a00070.html", "d2/db1/a00070" ],
    [ "AuxinGrowth", "d0/d89/a00071.html", "d0/d89/a00071" ],
    [ "BasicAuxin", "d6/d2c/a00072.html", "d6/d2c/a00072" ],
    [ "Geometric", "d1/d6e/a00073.html", "d1/d6e/a00073" ],
    [ "Meinhardt", "db/db6/a00074.html", "db/db6/a00074" ],
    [ "NoOp", "d9/da5/a00075.html", "d9/da5/a00075" ],
    [ "SmithPhyllotaxis", "db/df8/a00076.html", "db/df8/a00076" ],
    [ "Wortel", "d1/d09/a00077.html", "d1/d09/a00077" ],
    [ "WrapperModel", "d9/da4/a00078.html", "d9/da4/a00078" ]
];