var a00385 =
[
    [ "ItemType", "d5/d05/a00385.html#a4404ada30b1925be57d3cd283fe84f67", [
      [ "RootType", "d5/d05/a00385.html#a4404ada30b1925be57d3cd283fe84f67ab69ea67cd0f153ef92815b31b1420d6d", null ],
      [ "ProjectType", "d5/d05/a00385.html#a4404ada30b1925be57d3cd283fe84f67afbcd19911cf28e820e12f1d311fbca4e", null ],
      [ "FileType", "d5/d05/a00385.html#a4404ada30b1925be57d3cd283fe84f67ab3a1cc7e64a759996e3dd90a2927bb20", null ]
    ] ],
    [ "~WorkspaceQtModel", "d5/d05/a00385.html#a71e4e94137bf6f55c281439a2cb476ba", null ],
    [ "Close", "d5/d05/a00385.html#abd811cc03c0cb9f4aa16216695f79bbe", null ],
    [ "Close", "d5/d05/a00385.html#a7936baa761474f1be52dd2f59b233e5b", null ],
    [ "columnCount", "d5/d05/a00385.html#a4d73f1000e40a9248f4788169f2345a8", null ],
    [ "Create", "d5/d05/a00385.html#a516a19547c819ebb0aee066a193aa343", null ],
    [ "data", "d5/d05/a00385.html#abd7cd022909d3f610ea158fa70463375", null ],
    [ "flags", "d5/d05/a00385.html#a37ef33a1bba81b91723f78266f92e69c", null ],
    [ "GetContextMenuActions", "d5/d05/a00385.html#a5354ce9a3c224a298055e190e8fe0b61", null ],
    [ "GetName", "d5/d05/a00385.html#a7264f654fcdaea203b3dd7611f552c34", null ],
    [ "GetType", "d5/d05/a00385.html#afba58959a209d51be8944abec559e3bd", null ],
    [ "headerData", "d5/d05/a00385.html#a3789c3f40bc0d0d58314b66412f89690", null ],
    [ "index", "d5/d05/a00385.html#afe84a7615cc13a7fb01986a3f4a5b53d", null ],
    [ "insertRow", "d5/d05/a00385.html#a55faf0e4e39608a70bf3bc209659842f", null ],
    [ "insertRows", "d5/d05/a00385.html#ae3b641ce5b22f7918b6df6d5e0294e35", null ],
    [ "IsOpened", "d5/d05/a00385.html#a44ce3978e140e62cf8f47adbdf3b5e82", null ],
    [ "Open", "d5/d05/a00385.html#afff4413f86f7204f0086db7c1d99dac2", null ],
    [ "parent", "d5/d05/a00385.html#a33afce51eeef676be49dfc9a0a676623", null ],
    [ "removeRow", "d5/d05/a00385.html#a6bba58c4ff37503afa2430cc35469fb3", null ],
    [ "removeRows", "d5/d05/a00385.html#ac07f35027021dbbecbc94617c0276ec6", null ],
    [ "rowCount", "d5/d05/a00385.html#aabeb05090c8cdd62683512640d391179", null ],
    [ "Workspace", "d5/d05/a00385.html#ac71c71b85d0297b088b397dcfed11959", null ]
];