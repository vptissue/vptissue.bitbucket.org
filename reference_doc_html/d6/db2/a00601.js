var a00601 =
[
    [ "ExplorationTask", "d1/db7/a00157.html", "d1/db7/a00157" ],
    [ "TaskState", "d6/db2/a00601.html#a26cc0a5ce0e2b7c2f7d01f2a1cd72bd0", [
      [ "Waiting", "d6/db2/a00601.html#a26cc0a5ce0e2b7c2f7d01f2a1cd72bd0a5706de961fb376d701be6e7762d8b09c", null ],
      [ "Running", "d6/db2/a00601.html#a26cc0a5ce0e2b7c2f7d01f2a1cd72bd0a5bda814c4aedb126839228f1a3d92f09", null ],
      [ "Finished", "d6/db2/a00601.html#a26cc0a5ce0e2b7c2f7d01f2a1cd72bd0a8f3d10eb21bd36347c258679eba9e92b", null ],
      [ "Cancelled", "d6/db2/a00601.html#a26cc0a5ce0e2b7c2f7d01f2a1cd72bd0aa149e85a44aeec9140e92733d9ed694e", null ],
      [ "Failed", "d6/db2/a00601.html#a26cc0a5ce0e2b7c2f7d01f2a1cd72bd0ad7c8c85bf79bbe1b7188497c32c3b0ca", null ]
    ] ]
];