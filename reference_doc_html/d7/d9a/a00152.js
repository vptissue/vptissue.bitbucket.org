var a00152 =
[
    [ "Connection", "d7/d9a/a00152.html#af2644a5e5ed5a7cbc100f770d0b9dc7e", null ],
    [ "~Connection", "d7/d9a/a00152.html#aaab28ae00a384553320744fbeb9348b9", null ],
    [ "ConnectionClosed", "d7/d9a/a00152.html#a6f80f767640078a06c044d3478557b52", null ],
    [ "Error", "d7/d9a/a00152.html#a9fc6cca3322fe85cede35fe21d534712", null ],
    [ "GetPeerAddress", "d7/d9a/a00152.html#aa92468c097fb1bad9faa73e551787184", null ],
    [ "GetPeerPort", "d7/d9a/a00152.html#acd76e00ec8036902e30bd97298e3c48c", null ],
    [ "IsConnected", "d7/d9a/a00152.html#a531b804b7c44ea4b4f69ada9b4ee3c5c", null ],
    [ "ReceivedMessage", "d7/d9a/a00152.html#a24317bc0c4b752fcc89ea7334dc441fb", null ],
    [ "SendMessage", "d7/d9a/a00152.html#ab36fb08b04fc67517979d85bd432720f", null ]
];