var a00168 =
[
    [ "Protocol", "dd/da7/a00168.html#aa66b04ee201ffcba53ca8029538d2c00", null ],
    [ "Protocol", "dd/da7/a00168.html#a88d2a8a34ce8bef7a317335c8ae92a44", null ],
    [ "~Protocol", "dd/da7/a00168.html#a786120183d58d12cb9485c357b8afa2f", null ],
    [ "Ended", "dd/da7/a00168.html#ab614490af8b95c13217665d2ab946bd1", null ],
    [ "Error", "dd/da7/a00168.html#a904439c7f58d0ae589be9f9ee6e772ea", null ],
    [ "GetClientIP", "dd/da7/a00168.html#a3e523f1432ca2b9aa1141325432a451e", null ],
    [ "GetClientPort", "dd/da7/a00168.html#a7fef51454c2bd4216a65815c2aa43560", null ],
    [ "IsConnected", "dd/da7/a00168.html#a6f6cf42f0856d72172aec93914acf999", null ],
    [ "ReceivePtree", "dd/da7/a00168.html#a4ffaa492267120d9caaabcf376f9ed83", null ],
    [ "SendPtree", "dd/da7/a00168.html#a86c90ab4ced81de55861a1538ad712e5", null ]
];