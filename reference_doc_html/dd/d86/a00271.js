var a00271 =
[
    [ "Duration", "dd/d86/a00271.html#a20cb34c2e31ed1cdcc7ea0268778faf0", null ],
    [ "Clear", "dd/d86/a00271.html#ae3fb830816383563bb029d123f8cc319", null ],
    [ "GetCount", "dd/d86/a00271.html#aba703e6724490b6eb050ee530589b3d9", null ],
    [ "GetCumulative", "dd/d86/a00271.html#ab572259c3b8f26e651328b735d615a64", null ],
    [ "GetMean", "dd/d86/a00271.html#ae5600d7fb22e95dc2647935392c209d8", null ],
    [ "GetMinimum", "dd/d86/a00271.html#a17c14ff3aa5a79269a41b3253664babc", null ],
    [ "GetNames", "dd/d86/a00271.html#a1b414c43fa19e240c791aa32f3de0aa4", null ],
    [ "GetRecord", "dd/d86/a00271.html#aa052673347a6e1e0e1351ae84e2b9798", null ],
    [ "GetRecords", "dd/d86/a00271.html#a97dacea5463b5f9b61c66bb2182ea46c", null ],
    [ "GetRecords", "dd/d86/a00271.html#af069e730025e9757634ccac7313e1aeb", null ],
    [ "GetStandardDeviation", "dd/d86/a00271.html#ae833b581983a1545c3960ad92076c05b", null ],
    [ "IsPresent", "dd/d86/a00271.html#a3eaa2b34f769eca00a69b05e6b40eed4", null ],
    [ "Merge", "dd/d86/a00271.html#a128c718df3cb983891d4e1c30021e175", null ],
    [ "Merge", "dd/d86/a00271.html#ad53ad03a04e4f885a82cc84872e3d84e", null ],
    [ "Record", "dd/d86/a00271.html#a5ba54b6990e7ccbb8f6fe6ea4448d4d0", null ]
];