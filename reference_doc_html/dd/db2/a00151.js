var a00151 =
[
    [ "ClientProtocol", "dd/db2/a00151.html#aab191eafea2c772aa89212c3f987e387", null ],
    [ "~ClientProtocol", "dd/db2/a00151.html#a2128feb1fb9c2d73831ccb936652c565", null ],
    [ "DeleteExploration", "dd/db2/a00151.html#a18aad23543c2d9f06775dd6f757189ca", null ],
    [ "ExplorationDeleted", "dd/db2/a00151.html#af92dd496a1583338d22f48b5b8a465c0", null ],
    [ "ExplorationNames", "dd/db2/a00151.html#af6d1b8c60137dc6910bf5f2e36045190", null ],
    [ "ExplorationStatus", "dd/db2/a00151.html#a1d8c2af526273478daa3abc996cbfda5", null ],
    [ "ReceivePtree", "dd/db2/a00151.html#a9d309ef2f0c4ba5dc08f39c5c29c34e8", null ],
    [ "RequestExplorationNames", "dd/db2/a00151.html#a447744ddaa43f73c58b4fdbdeca08d3f", null ],
    [ "RestartTask", "dd/db2/a00151.html#a425f1d0e3689d1898eb2c4a610996db1", null ],
    [ "SendExploration", "dd/db2/a00151.html#a0707ffbfce88703d3e8b03a81e76bcd4", null ],
    [ "StopTask", "dd/db2/a00151.html#a813e06488ef17fd6e207b13e7fff9f10", null ],
    [ "SubscribeUpdates", "dd/db2/a00151.html#a6b86cadcbbb2fd8fb4a9a91a50fabae6", null ],
    [ "UnsubscribeUpdates", "dd/db2/a00151.html#ae8a361449e564e8ac2b97278d10d452e", null ]
];