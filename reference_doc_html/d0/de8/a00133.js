var a00133 =
[
    [ "CalculateArea", "d0/de8/a00133.html#a5675fda00514a0a45ced657bdb1eb6cd", null ],
    [ "ClipPolygon", "d0/de8/a00133.html#a8613eb041b94a538e64a176712ba5a1b", null ],
    [ "Counterclockwise", "d0/de8/a00133.html#a913752153ae3251373d5f0c77dcc2f52", null ],
    [ "IsClockwise", "d0/de8/a00133.html#a64b23fb66c57872102435d2691ad0cb6", null ],
    [ "IsSimplePolygon", "d0/de8/a00133.html#a791905fb04c05e5e2795e53777030ccd", null ],
    [ "OpenPolygon", "d0/de8/a00133.html#af5bd8d85e597c7d08bfd2efed1e34472", null ],
    [ "SlicePolygon", "d0/de8/a00133.html#ac78693a485fc0186492f8887763efdc6", null ]
];