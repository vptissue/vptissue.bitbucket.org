var a00343 =
[
    [ "CopyNonExistingChildren", "d0/dc5/a00343.html#a99cc0f9ae572cec9ebe456ccfaf07c42", null ],
    [ "CopyStructure", "d0/dc5/a00343.html#aecca0e34645ccc358e3e136bec6d4b14", null ],
    [ "FillPTree", "d0/dc5/a00343.html#ab08f38a5562b74622b20ccabab681698", null ],
    [ "Flatten", "d0/dc5/a00343.html#a7e10901f3efc870081628357399eb376", null ],
    [ "GetIndexedChild", "d0/dc5/a00343.html#a5239b5fbbf62f74fa40f96cb005a53cb", null ],
    [ "PutIndexedChild", "d0/dc5/a00343.html#aedd7418e424882a91b32203907d5a3a1", null ],
    [ "RemoveNonExistingChildren", "d0/dc5/a00343.html#a2e90aefa2954164ee0a436b061c8888a", null ]
];