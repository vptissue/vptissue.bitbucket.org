var files =
[
    [ "AppController.cpp", "d7/db9/a00426.html", null ],
    [ "AppController.h", "da/db6/a00427.html", [
      [ "AppController", "d6/d9c/a00352.html", "d6/d9c/a00352" ]
    ] ],
    [ "AppCustomizer.cpp", "d4/dd3/a00428.html", null ],
    [ "AppCustomizer.h", "d4/d53/a00429.html", [
      [ "AppCustomizer", "d5/df1/a00206.html", "d5/df1/a00206" ]
    ] ],
    [ "AreaMoment.h", "dc/d0c/a00430.html", "dc/d0c/a00430" ],
    [ "AreaThresholdBased.cpp", "df/d70/a00431.html", null ],
    [ "AreaThresholdBased.h", "d9/dd5/a00432.html", [
      [ "AreaThresholdBased", "d1/d51/a00079.html", "d1/d51/a00079" ]
    ] ],
    [ "array3.h", "dd/d07/a00433.html", "dd/d07/a00433" ],
    [ "ArrowItem.cpp", "d1/d7e/a00434.html", null ],
    [ "ArrowItem.h", "d0/dc5/a00435.html", [
      [ "ArrowItem", "d8/d00/a00186.html", "d8/d00/a00186" ]
    ] ],
    [ "AttributeContainer.cpp", "d4/d82/a00436.html", null ],
    [ "AttributeContainer.h", "d4/da8/a00437.html", "d4/da8/a00437" ],
    [ "AttributeStore.cpp", "dd/dfd/a00438.html", "dd/dfd/a00438" ],
    [ "AttributeStore.h", "d9/d3a/a00439.html", "d9/d3a/a00439" ],
    [ "cell_daughters/Auxin.cpp", "de/df8/a00440.html", null ],
    [ "cell_housekeep/Auxin.cpp", "d6/d66/a00441.html", null ],
    [ "cell_daughters/Auxin.h", "db/d34/a00442.html", [
      [ "Auxin", "df/d03/a00062.html", "df/d03/a00062" ]
    ] ],
    [ "cell_housekeep/Auxin.h", "d0/dc4/a00443.html", [
      [ "Auxin", "d2/db1/a00070.html", "d2/db1/a00070" ]
    ] ],
    [ "cell2cell_transport/AuxinGrowth.cpp", "d3/dea/a00444.html", null ],
    [ "cell_chemistry/AuxinGrowth.cpp", "dd/d47/a00445.html", null ],
    [ "cell_housekeep/AuxinGrowth.cpp", "d3/def/a00446.html", null ],
    [ "cell_split/AuxinGrowth.cpp", "de/d84/a00447.html", null ],
    [ "wall_chemistry/AuxinGrowth.cpp", "de/dfe/a00448.html", null ],
    [ "cell2cell_transport/AuxinGrowth.h", "d9/d10/a00449.html", [
      [ "AuxinGrowth", "df/d13/a00085.html", "df/d13/a00085" ]
    ] ],
    [ "cell_chemistry/AuxinGrowth.h", "de/df4/a00450.html", [
      [ "AuxinGrowth", "d5/df9/a00044.html", "d5/df9/a00044" ]
    ] ],
    [ "cell_housekeep/AuxinGrowth.h", "d9/d69/a00451.html", [
      [ "AuxinGrowth", "d0/d89/a00071.html", "d0/d89/a00071" ]
    ] ],
    [ "cell_split/AuxinGrowth.h", "d1/d0b/a00452.html", [
      [ "AuxinGrowth", "da/dec/a00080.html", "da/dec/a00080" ]
    ] ],
    [ "wall_chemistry/AuxinGrowth.h", "da/dc3/a00453.html", [
      [ "AuxinGrowth", "d8/d87/a00118.html", "d8/d87/a00118" ]
    ] ],
    [ "AuxinPIN1.cpp", "d7/d9f/a00454.html", null ],
    [ "AuxinPIN1.h", "dd/d63/a00455.html", [
      [ "AuxinPIN1", "d1/dea/a00054.html", "d1/dea/a00054" ]
    ] ],
    [ "BackgroundDialog.cpp", "de/d87/a00456.html", null ],
    [ "BackgroundDialog.h", "d1/dbd/a00457.html", [
      [ "BackgroundDialog", "df/dc8/a00122.html", "df/dc8/a00122" ]
    ] ],
    [ "cell2cell_transport/Basic.cpp", "da/d65/a00458.html", null ],
    [ "wall_chemistry/Basic.cpp", "d2/d91/a00459.html", null ],
    [ "cell2cell_transport/Basic.h", "db/db1/a00460.html", [
      [ "Basic", "da/d2a/a00086.html", "da/d2a/a00086" ]
    ] ],
    [ "wall_chemistry/Basic.h", "d5/d5f/a00461.html", [
      [ "Basic", "df/d6a/a00119.html", "df/d6a/a00119" ]
    ] ],
    [ "BasicAuxin.cpp", "de/dcb/a00462.html", null ],
    [ "BasicAuxin.h", "d3/d5d/a00463.html", [
      [ "BasicAuxin", "d6/d2c/a00072.html", "d6/d2c/a00072" ]
    ] ],
    [ "BasicPIN.cpp", "d2/d9d/a00464.html", null ],
    [ "BasicPIN.h", "db/de0/a00465.html", [
      [ "BasicPIN", "d1/d6f/a00063.html", "d1/d6f/a00063" ]
    ] ],
    [ "BitmapFormat.cpp", "db/d2c/a00466.html", null ],
    [ "BitmapFormat.h", "d7/d5e/a00467.html", [
      [ "BitmapFormat", "d6/daa/a00187.html", "d6/daa/a00187" ]
    ] ],
    [ "BitmapGraphicsExporter.cpp", "df/d38/a00468.html", null ],
    [ "BitmapGraphicsExporter.h", "dc/ddf/a00469.html", [
      [ "BitmapGraphicsExporter", "da/d15/a00188.html", "da/d15/a00188" ]
    ] ],
    [ "BitmapGraphicsPreferences.h", "d0/d29/a00470.html", [
      [ "BitmapGraphicsPreferences", "da/deb/a00189.html", "da/deb/a00189" ]
    ] ],
    [ "cell2cell_transport/Blad.cpp", "d4/d4b/a00471.html", null ],
    [ "cell_chemistry/Blad.cpp", "d7/db5/a00472.html", null ],
    [ "cell_color/Blad.cpp", "dc/d8e/a00473.html", null ],
    [ "cell_daughters/Blad.cpp", "d5/d26/a00474.html", null ],
    [ "cell_housekeep/Blad.cpp", "d1/d92/a00475.html", null ],
    [ "cell_split/Blad.cpp", "d9/d34/a00476.html", null ],
    [ "cell2cell_transport/Blad.h", "d9/dca/a00477.html", [
      [ "Blad", "d5/db6/a00042.html", "d5/db6/a00042" ]
    ] ],
    [ "cell_chemistry/Blad.h", "d9/dea/a00478.html", [
      [ "Blad", "d1/dc6/a00037.html", "d1/dc6/a00037" ]
    ] ],
    [ "cell_color/Blad.h", "d9/d4a/a00479.html", [
      [ "Blad", "d8/d41/a00038.html", "d8/d41/a00038" ]
    ] ],
    [ "cell_daughters/Blad.h", "d9/dc5/a00480.html", [
      [ "Blad", "d3/de7/a00039.html", "d3/de7/a00039" ]
    ] ],
    [ "cell_housekeep/Blad.h", "d3/d58/a00481.html", [
      [ "Blad", "db/d3a/a00040.html", "db/d3a/a00040" ]
    ] ],
    [ "cell_split/Blad.h", "db/d8e/a00482.html", [
      [ "Blad", "dd/d32/a00041.html", "dd/d32/a00041" ]
    ] ],
    [ "BoundaryType.h", "dc/da0/a00483.html", "dc/da0/a00483" ],
    [ "CBMBuilder.cpp", "d8/d89/a00484.html", null ],
    [ "CBMBuilder.h", "d6/d1a/a00485.html", [
      [ "CBMBuilder", "d4/d04/a00255.html", "d4/d04/a00255" ]
    ] ],
    [ "Cell.cpp", "db/df1/a00486.html", "db/df1/a00486" ],
    [ "Cell.h", "dd/d9c/a00487.html", "dd/d9c/a00487" ],
    [ "CellAttributes.cpp", "d4/ddf/a00488.html", "d4/ddf/a00488" ],
    [ "CellAttributes.h", "da/d05/a00489.html", "da/d05/a00489" ],
    [ "CellDivider.cpp", "d7/daa/a00490.html", null ],
    [ "CellDivider.h", "d6/d60/a00491.html", [
      [ "CellDivider", "dc/d6a/a00261.html", "dc/d6a/a00261" ]
    ] ],
    [ "CellHousekeeper.cpp", "da/d7e/a00492.html", null ],
    [ "CellHousekeeper.h", "d9/da5/a00493.html", [
      [ "CellHousekeeper", "d9/d8d/a00262.html", "d9/d8d/a00262" ]
    ] ],
    [ "CellType.h", "d5/d0f/a00494.html", "d5/d0f/a00494" ],
    [ "ChainHull.cpp", "db/dc4/a00495.html", "db/dc4/a00495" ],
    [ "ChainHull.h", "d3/dad/a00496.html", "d3/dad/a00496" ],
    [ "CheckableTreeModel.cpp", "d4/d1e/a00497.html", null ],
    [ "CheckableTreeModel.h", "d8/d57/a00498.html", [
      [ "CheckableTreeModel", "d0/d48/a00351.html", "d0/d48/a00351" ]
    ] ],
    [ "ChemBlue.cpp", "d8/d67/a00499.html", null ],
    [ "ChemBlue.h", "de/d4e/a00500.html", [
      [ "ChemBlue", "d4/d6d/a00055.html", "d4/d6d/a00055" ]
    ] ],
    [ "ChemGreen.cpp", "d8/db6/a00501.html", null ],
    [ "ChemGreen.h", "d0/da2/a00502.html", [
      [ "ChemGreen", "d9/d2e/a00056.html", "d9/d2e/a00056" ]
    ] ],
    [ "circular_iterator.h", "d4/de5/a00503.html", null ],
    [ "CircularIterator.h", "dc/d1c/a00504.html", "dc/d1c/a00504" ],
    [ "CircularIteratorImpl.h", "d7/d37/a00505.html", [
      [ "CircularIterator", "da/dd4/a00291.html", "da/dd4/a00291" ]
    ] ],
    [ "CliController.cpp", "d4/d1f/a00506.html", null ],
    [ "CliController.h", "d1/d69/a00507.html", [
      [ "CliController", "d7/d6e/a00190.html", "d7/d6e/a00190" ]
    ] ],
    [ "CliConverterTask.h", "dc/d30/a00508.html", "dc/d30/a00508" ],
    [ "Client.cpp", "df/db6/a00509.html", null ],
    [ "Client.h", "d6/d52/a00510.html", [
      [ "Client", "da/d1e/a00149.html", "da/d1e/a00149" ]
    ] ],
    [ "ClientHandler.cpp", "d6/d10/a00511.html", null ],
    [ "ClientHandler.h", "d4/dfb/a00512.html", [
      [ "ClientHandler", "d9/d7d/a00150.html", "d9/d7d/a00150" ]
    ] ],
    [ "ClientProtocol.cpp", "da/def/a00513.html", null ],
    [ "ClientProtocol.h", "d6/d90/a00514.html", [
      [ "ClientProtocol", "dd/db2/a00151.html", "dd/db2/a00151" ]
    ] ],
    [ "CliSimulationTask.h", "dc/d19/a00515.html", "dc/d19/a00515" ],
    [ "CliWorkspace.cpp", "d4/d40/a00516.html", null ],
    [ "CliWorkspace.h", "d8/dc7/a00517.html", [
      [ "CliWorkspace", "d4/d08/a00237.html", "d4/d08/a00237" ]
    ] ],
    [ "CliWorkspaceManager.cpp", "d5/dee/a00518.html", null ],
    [ "CliWorkspaceManager.h", "dd/d4d/a00519.html", [
      [ "CliWorkspaceManager", "dd/df4/a00193.html", "dd/df4/a00193" ]
    ] ],
    [ "clock_man.h", "d2/da3/a00520.html", null ],
    [ "ClockCLib.h", "d7/def/a00521.html", [
      [ "ClockCLib", "d5/d42/a00269.html", "d5/d42/a00269" ]
    ] ],
    [ "ClockTraits.h", "d8/d14/a00522.html", [
      [ "ClockTraits", "d8/d84/a00001.html", "d8/d84/a00001" ]
    ] ],
    [ "Blad/components/ComponentFactory.cpp", "d0/de7/a00523.html", "d0/de7/a00523" ],
    [ "Default/components/ComponentFactory.cpp", "d9/d24/a00524.html", "d9/d24/a00524" ],
    [ "Blad/components/ComponentFactory.h", "d7/db7/a00525.html", [
      [ "ComponentFactory", "d6/d42/a00043.html", "d6/d42/a00043" ]
    ] ],
    [ "Default/components/ComponentFactory.h", "d0/d35/a00526.html", [
      [ "ComponentFactory", "d1/d84/a00097.html", "d1/d84/a00097" ]
    ] ],
    [ "ComponentFactoryBase.h", "d9/d44/a00527.html", [
      [ "ComponentFactoryBase", "dc/d28/a00276.html", "dc/d28/a00276" ]
    ] ],
    [ "ComponentFactoryProxy.cpp", "d5/d81/a00528.html", null ],
    [ "ComponentFactoryProxy.h", "d1/d0b/a00529.html", [
      [ "ComponentFactoryProxy", "d6/d4b/a00277.html", "d6/d4b/a00277" ]
    ] ],
    [ "ComponentInterfaces.h", "da/d7e/a00530.html", "da/d7e/a00530" ],
    [ "ComponentTraits.h", "d0/d2a/a00531.html", [
      [ "CellChemistryTag", "d7/d2f/a00258.html", null ],
      [ "CellColorTag", "d0/d2f/a00259.html", null ],
      [ "CellDaughtersTag", "dd/dd7/a00260.html", null ],
      [ "CellHousekeepTag", "df/da9/a00263.html", null ],
      [ "CellSplitTag", "d3/dcc/a00264.html", null ],
      [ "CellToCellTransportBoundaryTag", "da/d53/a00265.html", null ],
      [ "CellToCellTransportTag", "d5/d35/a00266.html", null ],
      [ "ComponentTraits", "dc/d5d/a00278.html", null ],
      [ "ComponentTraits< CellChemistryTag >", "d2/d82/a00279.html", "d2/d82/a00279" ],
      [ "ComponentTraits< CellColorTag >", "d0/dcd/a00280.html", "d0/dcd/a00280" ],
      [ "ComponentTraits< CellDaughtersTag >", "d2/d04/a00281.html", "d2/d04/a00281" ],
      [ "ComponentTraits< CellHousekeepTag >", "dc/dc9/a00282.html", "dc/dc9/a00282" ],
      [ "ComponentTraits< CellSplitTag >", "dd/dfe/a00283.html", "dd/dfe/a00283" ],
      [ "ComponentTraits< CellToCellTransportBoundaryTag >", "db/dc3/a00284.html", "db/dc3/a00284" ],
      [ "ComponentTraits< CellToCellTransportTag >", "d3/d58/a00285.html", "d3/d58/a00285" ],
      [ "ComponentTraits< DeltaHamiltonianTag >", "d5/d7b/a00286.html", "d5/d7b/a00286" ],
      [ "ComponentTraits< HamiltonianTag >", "d6/d7f/a00287.html", "d6/d7f/a00287" ],
      [ "ComponentTraits< MoveGeneratorTag >", "da/ddc/a00288.html", "da/ddc/a00288" ],
      [ "ComponentTraits< TimeEvolverTag >", "d4/d66/a00289.html", "d4/d66/a00289" ],
      [ "ComponentTraits< WallChemistryTag >", "de/d25/a00290.html", "de/d25/a00290" ],
      [ "DeltaHamiltonianTag", "d3/d4a/a00297.html", null ],
      [ "HamiltonianTag", "d3/d07/a00305.html", null ],
      [ "MoveGeneratorTag", "dd/db3/a00313.html", null ],
      [ "TimeEvolverTag", "d4/dce/a00337.html", null ],
      [ "WallChemistryTag", "db/d10/a00350.html", null ]
    ] ],
    [ "Compressor.cpp", "d7/d41/a00532.html", null ],
    [ "Compressor.h", "db/d44/a00533.html", [
      [ "Compressor", "d6/d22/a00245.html", "d6/d22/a00245" ]
    ] ],
    [ "Connection.cpp", "dc/d20/a00534.html", "dc/d20/a00534" ],
    [ "Connection.h", "d7/d84/a00535.html", [
      [ "Connection", "d7/d9a/a00152.html", "d7/d9a/a00152" ]
    ] ],
    [ "constants.h", "d4/d33/a00536.html", "d4/d33/a00536" ],
    [ "ConstCircularIterator.h", "d8/d6e/a00537.html", "d8/d6e/a00537" ],
    [ "ConversionList.cpp", "dd/d5a/a00538.html", null ],
    [ "ConversionList.h", "d3/d87/a00539.html", [
      [ "ConversionList", "dc/d57/a00194.html", "dc/d57/a00194" ],
      [ "EntryType", "d3/d2a/a00195.html", "d3/d2a/a00195" ]
    ] ],
    [ "ConverterWindow.cpp", "d4/d79/a00540.html", null ],
    [ "ConverterWindow.h", "dd/d72/a00541.html", [
      [ "ConverterWindow", "d7/ded/a00196.html", "d7/ded/a00196" ]
    ] ],
    [ "CopyAttributesDialog.cpp", "d0/ddb/a00542.html", null ],
    [ "CopyAttributesDialog.h", "d4/d9c/a00543.html", [
      [ "CopyAttributesDialog", "de/d36/a00123.html", "de/d36/a00123" ]
    ] ],
    [ "CoreData.h", "d2/ddf/a00544.html", [
      [ "CoreData", "d6/d82/a00293.html", "d6/d82/a00293" ]
    ] ],
    [ "CoupledCliController.cpp", "d8/deb/a00545.html", null ],
    [ "CoupledCliController.h", "dc/d2b/a00546.html", [
      [ "CoupledCliController", "de/db1/a00197.html", "de/db1/a00197" ]
    ] ],
    [ "CoupledSim.cpp", "d9/d8b/a00547.html", null ],
    [ "CoupledSim.h", "dd/de6/a00548.html", [
      [ "CoupledSim", "d4/d5b/a00294.html", "d4/d5b/a00294" ]
    ] ],
    [ "CoupledSimEvent.h", "d5/d21/a00549.html", [
      [ "CoupledSimEvent", "db/da2/a00300.html", "db/da2/a00300" ]
    ] ],
    [ "CouplerFactories.cpp", "de/de7/a00550.html", null ],
    [ "CouplerFactories.h", "d9/d87/a00551.html", [
      [ "CouplerFactories", "d9/d5e/a00295.html", "d9/d5e/a00295" ]
    ] ],
    [ "CSRMatrix.h", "dd/de9/a00552.html", [
      [ "CSRMatrix", "db/d7b/a00296.html", "db/d7b/a00296" ]
    ] ],
    [ "CsvExporter.cpp", "d0/d79/a00553.html", null ],
    [ "CsvExporter.h", "dc/df9/a00554.html", [
      [ "CsvExporter", "d7/da0/a00198.html", "d7/da0/a00198" ]
    ] ],
    [ "CsvFormat.h", "d8/d75/a00555.html", [
      [ "CsvFormat", "d8/d72/a00199.html", "d8/d72/a00199" ]
    ] ],
    [ "CsvGzExporter.cpp", "d6/d95/a00556.html", null ],
    [ "CsvGzExporter.h", "d8/df0/a00557.html", [
      [ "CsvGzExporter", "d3/db1/a00200.html", "d3/db1/a00200" ]
    ] ],
    [ "CsvGzFormat.h", "dc/dea/a00558.html", [
      [ "CsvGzFormat", "dd/de3/a00201.html", "dd/de3/a00201" ]
    ] ],
    [ "CumulativeRecords.h", "d2/d50/a00559.html", "d2/d50/a00559" ],
    [ "DHelper.h", "d9/d19/a00560.html", [
      [ "DHelper", "d1/d44/a00098.html", "d1/d44/a00098" ]
    ] ],
    [ "DiamondTile.cpp", "d2/d50/a00561.html", null ],
    [ "DiamondTile.h", "d3/d2f/a00562.html", [
      [ "DiamondTile", "da/d72/a00124.html", "da/d72/a00124" ]
    ] ],
    [ "DirectedNormal.h", "dc/dae/a00563.html", [
      [ "DirectedNormal", "d1/dd6/a00108.html", "d1/dd6/a00108" ]
    ] ],
    [ "DirectedUniform.h", "d0/dc2/a00564.html", [
      [ "DirectedUniform", "dd/d42/a00109.html", "dd/d42/a00109" ]
    ] ],
    [ "directories.doxy", "d8/dd2/a00565.html", null ],
    [ "DurstenfeldShuffle.cpp", "d0/d3e/a00566.html", null ],
    [ "DurstenfeldShuffle.h", "d2/dd7/a00567.html", [
      [ "DurstenfeldShuffle", "d9/d0d/a00298.html", "d9/d0d/a00298" ]
    ] ],
    [ "Edge.cpp", "dc/d40/a00568.html", null ],
    [ "Edge.h", "d4/d9e/a00569.html", "d4/d9e/a00569" ],
    [ "EditableCellItem.cpp", "d8/da5/a00570.html", null ],
    [ "EditableCellItem.h", "df/d3a/a00571.html", [
      [ "EditableCellItem", "dc/dfe/a00125.html", "dc/dfe/a00125" ]
    ] ],
    [ "EditableEdgeItem.cpp", "d6/d06/a00572.html", null ],
    [ "EditableEdgeItem.h", "d6/d21/a00573.html", [
      [ "EditableEdgeItem", "d7/d12/a00126.html", "d7/d12/a00126" ]
    ] ],
    [ "EditableItem.h", "d4/dbe/a00574.html", [
      [ "EditableItem", "df/dcd/a00127.html", "df/dcd/a00127" ]
    ] ],
    [ "EditableMesh.cpp", "d9/d42/a00575.html", null ],
    [ "EditableMesh.h", "de/d7c/a00576.html", [
      [ "EditableMesh", "d1/d88/a00128.html", "d1/d88/a00128" ]
    ] ],
    [ "EditableNodeItem.cpp", "d8/d9b/a00577.html", null ],
    [ "EditableNodeItem.h", "dd/d5c/a00578.html", [
      [ "EditableNodeItem", "d9/d47/a00129.html", "d9/d47/a00129" ]
    ] ],
    [ "EditControlLogic.cpp", "dc/d4a/a00579.html", null ],
    [ "EditControlLogic.h", "d3/de2/a00580.html", [
      [ "EditControlLogic", "d6/db3/a00130.html", "d6/db3/a00130" ]
    ] ],
    [ "EditorActions.cpp", "d8/dc8/a00581.html", null ],
    [ "EditorActions.h", "d4/d2f/a00582.html", [
      [ "EditorActions", "d7/d06/a00131.html", "d7/d06/a00131" ]
    ] ],
    [ "delta_hamiltonian/ElasticWall.cpp", "d5/d80/a00583.html", null ],
    [ "hamiltonian/ElasticWall.cpp", "d7/d21/a00584.html", null ],
    [ "delta_hamiltonian/ElasticWall.h", "dc/dac/a00585.html", [
      [ "ElasticWall", "da/d74/a00099.html", "da/d74/a00099" ]
    ] ],
    [ "hamiltonian/ElasticWall.h", "d6/d92/a00586.html", [
      [ "ElasticWall", "d5/d1e/a00103.html", "d5/d1e/a00103" ]
    ] ],
    [ "EnabledActions.cpp", "de/dde/a00587.html", null ],
    [ "EnabledActions.h", "d8/d2e/a00588.html", [
      [ "EnabledActions", "d6/de1/a00356.html", "d6/de1/a00356" ]
    ] ],
    [ "Exception.h", "d6/da3/a00589.html", [
      [ "Exception", "d3/d93/a00341.html", "d3/d93/a00341" ]
    ] ],
    [ "ExchangeCoupler.cpp", "d7/db1/a00590.html", null ],
    [ "ExchangeCoupler.h", "d9/dad/a00591.html", [
      [ "ExchangeCoupler", "d0/d61/a00302.html", "d0/d61/a00302" ]
    ] ],
    [ "Exploration.cpp", "d0/dec/a00592.html", null ],
    [ "Exploration.h", "d2/da3/a00593.html", [
      [ "Exploration", "da/d01/a00153.html", "da/d01/a00153" ]
    ] ],
    [ "ExplorationManager.cpp", "da/de4/a00594.html", null ],
    [ "ExplorationManager.h", "d8/de9/a00595.html", [
      [ "ExplorationManager", "d3/d65/a00154.html", "d3/d65/a00154" ]
    ] ],
    [ "ExplorationProgress.cpp", "dc/d25/a00596.html", null ],
    [ "ExplorationProgress.h", "d8/dbd/a00597.html", [
      [ "ExplorationProgress", "d3/df8/a00155.html", "d3/df8/a00155" ]
    ] ],
    [ "ExplorationSelection.cpp", "d7/dd2/a00598.html", null ],
    [ "ExplorationSelection.h", "d5/d8a/a00599.html", [
      [ "ExplorationSelection", "d2/d34/a00156.html", "d2/d34/a00156" ]
    ] ],
    [ "ExplorationTask.cpp", "d1/d8c/a00600.html", null ],
    [ "ExplorationTask.h", "d6/db2/a00601.html", "d6/db2/a00601" ],
    [ "ExplorationWizard.cpp", "d5/d0a/a00602.html", null ],
    [ "ExplorationWizard.h", "de/d94/a00603.html", [
      [ "ExplorationWizard", "d6/db8/a00158.html", "d6/db8/a00158" ]
    ] ],
    [ "ExportActions.cpp", "d6/d75/a00604.html", null ],
    [ "ExportActions.h", "d0/d1c/a00605.html", [
      [ "ExportActions", "df/dee/a00369.html", "df/dee/a00369" ]
    ] ],
    [ "ExporterFormat.h", "d8/d00/a00606.html", [
      [ "ExporterFormat", "d1/d7c/a00003.html", "d1/d7c/a00003" ]
    ] ],
    [ "Blad/components/cell2cell_transport/factories.cpp", "d9/d07/a00607.html", "d9/d07/a00607" ],
    [ "Blad/components/cell_chemistry/factories.cpp", "d4/d14/a00608.html", "d4/d14/a00608" ],
    [ "Blad/components/cell_color/factories.cpp", "db/d3f/a00609.html", "db/d3f/a00609" ],
    [ "Blad/components/cell_daughters/factories.cpp", "da/d6e/a00610.html", "da/d6e/a00610" ],
    [ "Blad/components/cell_housekeep/factories.cpp", "dd/dc0/a00611.html", "dd/dc0/a00611" ],
    [ "Blad/components/cell_split/factories.cpp", "d0/d8d/a00612.html", "d0/d8d/a00612" ],
    [ "Default/components/cell2cell_transport/factories.cpp", "d1/d6e/a00613.html", "d1/d6e/a00613" ],
    [ "Default/components/cell2cell_transport_boundary/factories.cpp", "d7/d80/a00614.html", "d7/d80/a00614" ],
    [ "Default/components/cell_chemistry/factories.cpp", "d5/dd8/a00615.html", "d5/dd8/a00615" ],
    [ "Default/components/cell_color/factories.cpp", "db/db5/a00616.html", "db/db5/a00616" ],
    [ "Default/components/cell_daughters/factories.cpp", "d4/dc4/a00617.html", "d4/dc4/a00617" ],
    [ "Default/components/cell_housekeep/factories.cpp", "de/d85/a00618.html", "de/d85/a00618" ],
    [ "Default/components/cell_split/factories.cpp", "d6/d6a/a00619.html", "d6/d6a/a00619" ],
    [ "Default/components/delta_hamiltonian/factories.cpp", "dc/d4d/a00620.html", "dc/d4d/a00620" ],
    [ "Default/components/hamiltonian/factories.cpp", "dc/db1/a00621.html", "dc/db1/a00621" ],
    [ "Default/components/move_generator/factories.cpp", "d0/db1/a00622.html", "d0/db1/a00622" ],
    [ "Default/components/time_evolver/factories.cpp", "df/d6f/a00623.html", "df/d6f/a00623" ],
    [ "Default/components/wall_chemistry/factories.cpp", "d3/dd6/a00624.html", "d3/dd6/a00624" ],
    [ "Blad/components/cell2cell_transport/factories.h", "d2/d34/a00625.html", null ],
    [ "Blad/components/cell_chemistry/factories.h", "d6/dfa/a00626.html", null ],
    [ "Blad/components/cell_color/factories.h", "d3/da3/a00627.html", null ],
    [ "Blad/components/cell_daughters/factories.h", "d1/d51/a00628.html", null ],
    [ "Blad/components/cell_housekeep/factories.h", "da/dd1/a00629.html", null ],
    [ "Blad/components/cell_split/factories.h", "dc/d66/a00630.html", null ],
    [ "Default/components/cell2cell_transport/factories.h", "d5/de3/a00631.html", null ],
    [ "Default/components/cell2cell_transport_boundary/factories.h", "d4/d2a/a00632.html", null ],
    [ "Default/components/cell_chemistry/factories.h", "d9/d1d/a00633.html", null ],
    [ "Default/components/cell_color/factories.h", "d7/de4/a00634.html", null ],
    [ "Default/components/cell_daughters/factories.h", "d4/d10/a00635.html", null ],
    [ "Default/components/cell_housekeep/factories.h", "db/ddc/a00636.html", null ],
    [ "Default/components/cell_split/factories.h", "de/dbb/a00637.html", null ],
    [ "Default/components/delta_hamiltonian/factories.h", "d1/daa/a00638.html", null ],
    [ "Default/components/hamiltonian/factories.h", "dd/db6/a00639.html", null ],
    [ "Default/components/move_generator/factories.h", "dc/d62/a00640.html", null ],
    [ "Default/components/time_evolver/factories.h", "db/d3a/a00641.html", null ],
    [ "Default/components/wall_chemistry/factories.h", "df/dcf/a00642.html", null ],
    [ "FileConversion.cpp", "de/d19/a00643.html", null ],
    [ "FileConversion.h", "d9/d0f/a00644.html", [
      [ "FileConversion", "db/d53/a00202.html", "db/d53/a00202" ]
    ] ],
    [ "FileConverterFormats.cpp", "d6/dcf/a00645.html", null ],
    [ "FileConverterFormats.h", "d6/d08/a00646.html", [
      [ "FileConverterFormats", "de/d79/a00203.html", "de/d79/a00203" ]
    ] ],
    [ "FileExploration.cpp", "d3/d8e/a00647.html", null ],
    [ "FileExploration.h", "dd/d1f/a00648.html", [
      [ "FileExploration", "de/db9/a00159.html", "de/db9/a00159" ]
    ] ],
    [ "FilesPage.cpp", "dd/d6b/a00649.html", null ],
    [ "FilesPage.h", "da/d36/a00650.html", [
      [ "FilesPage", "de/d38/a00160.html", "de/d38/a00160" ]
    ] ],
    [ "FileSystemWatcher.h", "de/d33/a00651.html", [
      [ "FileSystemWatcher", "dc/df3/a00424.html", "dc/df3/a00424" ]
    ] ],
    [ "FileViewer.h", "df/d37/a00652.html", [
      [ "FileViewer", "d0/d0b/a00004.html", "d0/d0b/a00004" ]
    ] ],
    [ "FileViewerPreferences.h", "de/ddd/a00653.html", [
      [ "FileViewerPreferences", "d1/d72/a00204.html", "d1/d72/a00204" ]
    ] ],
    [ "FunctionMap.h", "d9/d2a/a00654.html", [
      [ "FunctionMap", "dd/dad/a00005.html", "dd/dad/a00005" ]
    ] ],
    [ "GeoData.h", "d8/ddc/a00655.html", "d8/ddc/a00655" ],
    [ "Geom.cpp", "dc/df2/a00656.html", null ],
    [ "Geom.h", "da/dc3/a00657.html", [
      [ "Geom", "d8/d17/a00304.html", "d8/d17/a00304" ]
    ] ],
    [ "cell_housekeep/Geometric.cpp", "d9/db8/a00658.html", null ],
    [ "cell_split/Geometric.cpp", "dc/d91/a00659.html", null ],
    [ "cell_housekeep/Geometric.h", "d5/d4e/a00660.html", [
      [ "Geometric", "d1/d6e/a00073.html", "d1/d6e/a00073" ]
    ] ],
    [ "cell_split/Geometric.h", "d0/d50/a00661.html", [
      [ "Geometric", "d8/d53/a00081.html", "d8/d53/a00081" ]
    ] ],
    [ "GNUC_VERSION.h", "dc/df1/a00662.html", null ],
    [ "GraphicsPreferences.h", "d7/d5f/a00663.html", [
      [ "GraphicsPreferences", "d5/d13/a00205.html", "d5/d13/a00205" ]
    ] ],
    [ "Grow.cpp", "df/d46/a00664.html", null ],
    [ "Grow.h", "da/d46/a00665.html", [
      [ "Grow", "db/d57/a00112.html", "db/d57/a00112" ]
    ] ],
    [ "GuiWorkspace.cpp", "d9/d41/a00666.html", null ],
    [ "GuiWorkspace.h", "d2/d6e/a00667.html", [
      [ "GuiWorkspace", "da/d2e/a00238.html", "da/d2e/a00238" ]
    ] ],
    [ "HasUnsavedChanges.cpp", "d0/d5c/a00668.html", null ],
    [ "HasUnsavedChanges.h", "d8/d7a/a00669.html", [
      [ "HasUnsavedChanges", "df/df1/a00357.html", "df/df1/a00357" ],
      [ "HasUnsavedChangesDummy", "df/d73/a00358.html", "df/d73/a00358" ]
    ] ],
    [ "HasUnsavedChangesPrompt.cpp", "d1/d97/a00670.html", null ],
    [ "HasUnsavedChangesPrompt.h", "d8/d13/a00671.html", [
      [ "HasUnsavedChangesPrompt", "d4/def/a00359.html", "d4/def/a00359" ]
    ] ],
    [ "Hdf5Exporter.cpp", "d5/d40/a00672.html", null ],
    [ "Hdf5Exporter.h", "d4/d4f/a00673.html", [
      [ "Hdf5Exporter", "dc/d31/a00208.html", "dc/d31/a00208" ]
    ] ],
    [ "Hdf5File.cpp", "d8/d73/a00674.html", null ],
    [ "Hdf5File.h", "d6/da1/a00675.html", [
      [ "Hdf5File", "d3/de5/a00209.html", "d3/de5/a00209" ]
    ] ],
    [ "Hdf5Format.cpp", "d5/d39/a00676.html", null ],
    [ "Hdf5Format.h", "dc/de4/a00677.html", [
      [ "Hdf5Format", "da/de7/a00210.html", "da/de7/a00210" ]
    ] ],
    [ "Hdf5Viewer.cpp", "da/d31/a00678.html", null ],
    [ "Hdf5Viewer.h", "d2/d85/a00679.html", [
      [ "Hdf5Viewer", "dd/ded/a00211.html", "dd/ded/a00211" ]
    ] ],
    [ "HexagonalTile.cpp", "d4/d9b/a00680.html", null ],
    [ "HexagonalTile.h", "d8/dba/a00681.html", [
      [ "HexagonalTile", "d5/deb/a00132.html", "d5/deb/a00132" ]
    ] ],
    [ "HHelper.h", "d7/dc3/a00682.html", [
      [ "HHelper", "d0/dc2/a00104.html", "d0/dc2/a00104" ]
    ] ],
    [ "Housekeep.cpp", "db/d77/a00683.html", null ],
    [ "Housekeep.h", "d3/db4/a00684.html", [
      [ "Housekeep", "d3/d00/a00113.html", "d3/d00/a00113" ]
    ] ],
    [ "HousekeepGrow.cpp", "d0/df1/a00685.html", null ],
    [ "HousekeepGrow.h", "df/d9e/a00686.html", [
      [ "HousekeepGrow", "d8/d17/a00114.html", "d8/d17/a00114" ]
    ] ],
    [ "hsv.h", "df/d1b/a00687.html", "df/d1b/a00687" ],
    [ "Hsv2Rgb.cpp", "d6/dd3/a00688.html", "d6/dd3/a00688" ],
    [ "Hsv2Rgb.h", "d1/d87/a00689.html", "d1/d87/a00689" ],
    [ "IConverterFormat.h", "da/dd4/a00690.html", [
      [ "IConverterFormat", "dd/dc1/a00212.html", "dd/dc1/a00212" ]
    ] ],
    [ "ICoupler.h", "db/dde/a00691.html", [
      [ "ICoupler", "d1/de3/a00306.html", "d1/de3/a00306" ]
    ] ],
    [ "IFactory.h", "d0/db6/a00692.html", [
      [ "IFactory", "d0/d65/a00360.html", "d0/d65/a00360" ]
    ] ],
    [ "IFile.h", "d3/dd2/a00693.html", [
      [ "IFile", "da/d9c/a00413.html", "da/d9c/a00413" ]
    ] ],
    [ "IHasPTreeState.h", "da/d3a/a00694.html", [
      [ "IHasPTreeState", "d7/ddb/a00361.html", "d7/ddb/a00361" ]
    ] ],
    [ "IndividualRecords.h", "d5/d5b/a00695.html", "d5/d5b/a00695" ],
    [ "InstallDirs.cpp", "df/d7d/a00696.html", null ],
    [ "InstallDirs.h", "d2/d74/a00697.html", [
      [ "InstallDirs", "db/dbc/a00395.html", "db/dbc/a00395" ]
    ] ],
    [ "IPreferences.h", "df/dbb/a00698.html", [
      [ "IPreferences", "d4/dbc/a00414.html", "d4/dbc/a00414" ]
    ] ],
    [ "IProject.h", "d8/d33/a00699.html", [
      [ "IProject", "d0/dd6/a00415.html", "d0/dd6/a00415" ],
      [ "WidgetCallback", "df/d15/a00416.html", "df/d15/a00416" ]
    ] ],
    [ "ISession.h", "d0/d15/a00700.html", [
      [ "ISession", "d7/d93/a00397.html", "d7/d93/a00397" ],
      [ "ExporterType", "d9/dd1/a00398.html", "d9/dd1/a00398" ]
    ] ],
    [ "ISimSession.h", "dd/d57/a00701.html", [
      [ "ISimSession", "dc/d28/a00223.html", "dc/d28/a00223" ]
    ] ],
    [ "ISweep.h", "dd/d85/a00702.html", [
      [ "ISweep", "d6/d7d/a00161.html", "d6/d7d/a00161" ]
    ] ],
    [ "IUserData.h", "d8/d86/a00703.html", [
      [ "IUserData", "d8/de7/a00417.html", "d8/de7/a00417" ]
    ] ],
    [ "IViewerNode.h", "d4/d6a/a00704.html", [
      [ "IViewerNode", "d7/d00/a00401.html", "d7/d00/a00401" ],
      [ "IViewerNodeWithParent", "de/d5e/a00007.html", "de/d5e/a00007" ]
    ] ],
    [ "IWorkspace.h", "d7/df1/a00705.html", [
      [ "IWorkspace", "de/df7/a00418.html", "de/df7/a00418" ],
      [ "ProjectMapEntry", "d3/d29/a00419.html", "d3/d29/a00419" ]
    ] ],
    [ "IWorkspaceFactory.h", "d8/dcb/a00706.html", [
      [ "IWorkspaceFactory", "db/d4f/a00420.html", "db/d4f/a00420" ]
    ] ],
    [ "ListSweep.cpp", "d1/d56/a00707.html", null ],
    [ "ListSweep.h", "da/d6b/a00708.html", [
      [ "ListSweep", "db/d44/a00162.html", "db/d44/a00162" ]
    ] ],
    [ "log_debug.h", "d7/d7e/a00709.html", "d7/d7e/a00709" ],
    [ "LogViewer.h", "d4/deb/a00710.html", [
      [ "LogViewer", "d1/de1/a00213.html", "d1/de1/a00213" ]
    ] ],
    [ "LogWindow.cpp", "de/d4c/a00711.html", null ],
    [ "LogWindow.h", "d3/dda/a00712.html", [
      [ "LogWindow", "de/dd7/a00362.html", "de/dd7/a00362" ]
    ] ],
    [ "LogWindowViewer.cpp", "d1/d04/a00713.html", null ],
    [ "LogWindowViewer.h", "df/d96/a00714.html", [
      [ "LogWindowViewer", "d3/dd1/a00214.html", "d3/dd1/a00214" ]
    ] ],
    [ "mainpage.doxy", "d3/d3b/a00715.html", null ],
    [ "math.h", "d2/d94/a00716.html", null ],
    [ "delta_hamiltonian/Maxwell.cpp", "d3/d82/a00717.html", null ],
    [ "hamiltonian/Maxwell.cpp", "dc/d8f/a00718.html", null ],
    [ "delta_hamiltonian/Maxwell.h", "dd/dd8/a00719.html", [
      [ "Maxwell", "d0/d61/a00100.html", "d0/d61/a00100" ]
    ] ],
    [ "hamiltonian/Maxwell.h", "dc/d14/a00720.html", [
      [ "Maxwell", "d3/d32/a00105.html", "d3/d32/a00105" ]
    ] ],
    [ "MBMBuilder.cpp", "dd/dc1/a00721.html", null ],
    [ "MBMBuilder.h", "db/d7c/a00722.html", [
      [ "MBMBuilder", "d0/da2/a00307.html", "d0/da2/a00307" ]
    ] ],
    [ "cell2cell_transport/Meinhardt.cpp", "de/de6/a00723.html", null ],
    [ "cell_chemistry/Meinhardt.cpp", "d0/d72/a00724.html", null ],
    [ "cell_color/Meinhardt.cpp", "d8/db9/a00725.html", null ],
    [ "cell_housekeep/Meinhardt.cpp", "d6/d59/a00726.html", null ],
    [ "wall_chemistry/Meinhardt.cpp", "dc/ddc/a00727.html", null ],
    [ "cell2cell_transport/Meinhardt.h", "df/d3b/a00728.html", [
      [ "Meinhardt", "d1/d1e/a00087.html", "d1/d1e/a00087" ]
    ] ],
    [ "cell_chemistry/Meinhardt.h", "d8/d52/a00729.html", [
      [ "Meinhardt", "d6/d97/a00045.html", "d6/d97/a00045" ]
    ] ],
    [ "cell_color/Meinhardt.h", "d2/db1/a00730.html", [
      [ "Meinhardt", "d1/d22/a00057.html", "d1/d22/a00057" ]
    ] ],
    [ "cell_housekeep/Meinhardt.h", "df/df7/a00731.html", [
      [ "Meinhardt", "db/db6/a00074.html", "db/db6/a00074" ]
    ] ],
    [ "wall_chemistry/Meinhardt.h", "d4/db8/a00732.html", [
      [ "Meinhardt", "d4/d98/a00120.html", "d4/d98/a00120" ]
    ] ],
    [ "MergedPreferences.cpp", "d4/d91/a00733.html", null ],
    [ "MergedPreferences.h", "d6/dc0/a00734.html", [
      [ "MergedPreferencesChanged", "de/d3d/a00409.html", "de/d3d/a00409" ],
      [ "MergedPreferences", "d2/de1/a00421.html", "d2/de1/a00421" ]
    ] ],
    [ "Mesh.cpp", "d0/dab/a00735.html", null ],
    [ "Mesh.h", "dc/d85/a00736.html", [
      [ "Mesh", "d5/d0f/a00308.html", "d5/d0f/a00308" ]
    ] ],
    [ "MeshCheck.cpp", "dd/db2/a00737.html", null ],
    [ "MeshCheck.h", "d1/da6/a00738.html", [
      [ "MeshCheck", "d2/d71/a00309.html", "d2/d71/a00309" ]
    ] ],
    [ "MeshDrawer.cpp", "da/df7/a00739.html", "da/df7/a00739" ],
    [ "MeshDrawer.h", "dc/dce/a00740.html", [
      [ "MeshDrawer", "d3/dd5/a00215.html", "d3/dd5/a00215" ]
    ] ],
    [ "MeshGeometry.cpp", "da/da3/a00741.html", null ],
    [ "MeshGeometry.h", "d4/d8f/a00742.html", [
      [ "MeshGeometry", "d1/d67/a00310.html", "d1/d67/a00310" ]
    ] ],
    [ "MeshState.cpp", "d0/d0a/a00743.html", null ],
    [ "MeshState.h", "d5/db3/a00744.html", [
      [ "MeshState", "d1/d5d/a00311.html", "d1/d5d/a00311" ]
    ] ],
    [ "MeshTopology.cpp", "d1/d79/a00745.html", null ],
    [ "MeshTopology.h", "d4/dfa/a00746.html", [
      [ "MeshTopology", "d7/df2/a00312.html", "d7/df2/a00312" ]
    ] ],
    [ "mode_manager.h", "d5/d68/a00747.html", "d5/d68/a00747" ],
    [ "delta_hamiltonian/ModifiedGC.cpp", "de/d3d/a00748.html", null ],
    [ "hamiltonian/ModifiedGC.cpp", "d5/de8/a00749.html", null ],
    [ "delta_hamiltonian/ModifiedGC.h", "d9/db0/a00750.html", [
      [ "ModifiedGC", "d4/d3a/a00101.html", "d4/d3a/a00101" ]
    ] ],
    [ "hamiltonian/ModifiedGC.h", "da/df2/a00751.html", [
      [ "ModifiedGC", "d7/d70/a00106.html", "d7/d70/a00106" ]
    ] ],
    [ "MyDockWidget.cpp", "dd/d5c/a00752.html", null ],
    [ "MyDockWidget.h", "dc/dd0/a00753.html", [
      [ "MyDockWidget", "d9/d9b/a00363.html", "d9/d9b/a00363" ]
    ] ],
    [ "MyFindDialog.cpp", "d8/d4e/a00754.html", null ],
    [ "MyFindDialog.h", "d2/ddd/a00755.html", [
      [ "MyFindDialog", "dc/d59/a00364.html", "dc/d59/a00364" ]
    ] ],
    [ "MyGraphicsView.cpp", "d9/d77/a00756.html", null ],
    [ "MyGraphicsView.h", "d3/d95/a00757.html", [
      [ "MyGraphicsView", "dd/db5/a00365.html", "dd/db5/a00365" ]
    ] ],
    [ "MyTreeView.cpp", "d2/d4a/a00758.html", null ],
    [ "MyTreeView.h", "d2/d87/a00759.html", [
      [ "MyTreeView", "d2/d3e/a00366.html", "d2/d3e/a00366" ]
    ] ],
    [ "namespaces.doxy", "d8/db4/a00760.html", null ],
    [ "NeighborNodes.cpp", "d3/d4f/a00761.html", "d3/d4f/a00761" ],
    [ "NeighborNodes.h", "df/d5f/a00762.html", "df/d5f/a00762" ],
    [ "NewProjectDialog.cpp", "d3/de3/a00763.html", null ],
    [ "NewProjectDialog.h", "d3/d2c/a00764.html", [
      [ "NewProjectDialog", "df/dc2/a00367.html", "df/dc2/a00367" ]
    ] ],
    [ "Node.cpp", "dc/de2/a00765.html", "dc/de2/a00765" ],
    [ "Node.h", "d4/d13/a00766.html", "d4/d13/a00766" ],
    [ "NodeAdvertiser.cpp", "d5/dae/a00767.html", null ],
    [ "NodeAdvertiser.h", "d8/d20/a00768.html", [
      [ "NodeAdvertiser", "dd/dae/a00163.html", "dd/dae/a00163" ]
    ] ],
    [ "NodeAttributes.cpp", "dd/d51/a00769.html", null ],
    [ "NodeAttributes.h", "d0/d7e/a00770.html", [
      [ "NodeAttributes", "d3/db8/a00316.html", "d3/db8/a00316" ]
    ] ],
    [ "NodeInserter.cpp", "dd/de5/a00771.html", null ],
    [ "NodeInserter.h", "d9/d9c/a00772.html", [
      [ "NodeInserter", "da/de0/a00317.html", "da/de0/a00317" ]
    ] ],
    [ "NodeItem.cpp", "d8/dba/a00773.html", null ],
    [ "NodeItem.h", "d3/dee/a00774.html", [
      [ "NodeItem", "d6/d43/a00216.html", "d6/d43/a00216" ]
    ] ],
    [ "NodeMover.cpp", "dc/d5d/a00775.html", null ],
    [ "NodeMover.h", "d9/dd4/a00776.html", [
      [ "NodeMover", "d2/d41/a00318.html", "d2/d41/a00318" ],
      [ "MetropolisInfo", "de/dab/a00319.html", "de/dab/a00319" ],
      [ "MoveInfo", "dd/de2/a00320.html", "dd/de2/a00320" ]
    ] ],
    [ "NodeProtocol.cpp", "d2/d02/a00777.html", null ],
    [ "NodeProtocol.h", "d6/d17/a00778.html", [
      [ "NodeProtocol", "db/ddf/a00164.html", "db/ddf/a00164" ]
    ] ],
    [ "NoOp.cpp", "d3/d95/a00779.html", null ],
    [ "cell2cell_transport/NoOp.h", "de/d42/a00780.html", [
      [ "NoOp", "db/ddf/a00088.html", "db/ddf/a00088" ]
    ] ],
    [ "cell_chemistry/NoOp.h", "d6/d86/a00781_source.html", null ],
    [ "cell_daughters/NoOp.h", "da/d96/a00782.html", [
      [ "NoOp", "df/d5f/a00064.html", "df/d5f/a00064" ]
    ] ],
    [ "cell_housekeep/NoOp.h", "dc/d0c/a00783.html", [
      [ "NoOp", "d9/da5/a00075.html", "d9/da5/a00075" ]
    ] ],
    [ "cell_split/NoOp.h", "d9/d00/a00784.html", [
      [ "NoOp", "d4/d63/a00082.html", "d4/d63/a00082" ]
    ] ],
    [ "wall_chemistry/NoOp.h", "d3/d00/a00785.html", [
      [ "NoOp", "d8/dfa/a00121.html", "d8/dfa/a00121" ]
    ] ],
    [ "odeint.h", "d0/d7d/a00786.html", null ],
    [ "OdeintFactories0Map.cpp", "d8/d7f/a00787.html", null ],
    [ "OdeintFactories0Map.h", "df/d0c/a00788.html", [
      [ "OdeintFactories0Map", "db/d53/a00321.html", "db/d53/a00321" ]
    ] ],
    [ "OdeintFactories2Map.cpp", "d8/d7d/a00789.html", null ],
    [ "OdeintFactories2Map.h", "d3/daf/a00790.html", [
      [ "OdeintFactories2Map", "de/de9/a00322.html", "de/de9/a00322" ]
    ] ],
    [ "OdeintFactory0.h", "d4/d45/a00791.html", [
      [ "OdeintFactory0", "de/d55/a00323.html", "de/d55/a00323" ]
    ] ],
    [ "OdeintFactory2.h", "d2/d5a/a00792.html", [
      [ "OdeintFactory2", "d4/d8f/a00324.html", "d4/d8f/a00324" ]
    ] ],
    [ "OdeintTraits.h", "d4/dd7/a00793.html", [
      [ "OdeintTraits", "de/dc8/a00325.html", "de/dc8/a00325" ]
    ] ],
    [ "PanAndZoomView.cpp", "d5/d12/a00794.html", null ],
    [ "PanAndZoomView.h", "dd/d7a/a00795.html", [
      [ "PanAndZoomView", "d8/d21/a00368.html", "d8/d21/a00368" ]
    ] ],
    [ "ParameterExploration.cpp", "d0/da2/a00796.html", null ],
    [ "ParameterExploration.h", "da/db8/a00797.html", [
      [ "ParameterExploration", "d0/d48/a00165.html", "d0/d48/a00165" ]
    ] ],
    [ "ParamPage.cpp", "de/d79/a00798.html", null ],
    [ "ParamPage.h", "d0/d7d/a00799.html", [
      [ "ParamPage", "d9/d38/a00166.html", "d9/d38/a00166" ]
    ] ],
    [ "parex_client.cpp", "df/d0e/a00800.html", null ],
    [ "parex_client_mode.h", "d7/d0a/a00801.html", [
      [ "ParexClientMode", "da/da0/a00009.html", "da/da0/a00009" ]
    ] ],
    [ "parex_node.cpp", "d5/dcd/a00802.html", null ],
    [ "parex_node_mode.h", "d5/d72/a00803.html", [
      [ "ParexNodeMode", "d7/dec/a00010.html", "d7/dec/a00010" ]
    ] ],
    [ "parex_server.cpp", "d1/dd1/a00804.html", null ],
    [ "parex_server_mode.h", "d7/dff/a00805.html", [
      [ "ParexServerMode", "da/d45/a00011.html", "da/d45/a00011" ]
    ] ],
    [ "PathPage.cpp", "d0/dad/a00806.html", null ],
    [ "PathPage.h", "df/dc7/a00807.html", [
      [ "PathPage", "dc/de6/a00167.html", "dc/de6/a00167" ]
    ] ],
    [ "PBMBuilder.cpp", "d7/dc8/a00808.html", null ],
    [ "PBMBuilder.h", "dd/d7e/a00809.html", [
      [ "PBMBuilder", "d8/d23/a00326.html", "d8/d23/a00326" ]
    ] ],
    [ "Perimeter.cpp", "d4/dfd/a00810.html", null ],
    [ "Perimeter.h", "dd/d07/a00811.html", [
      [ "Perimeter", "de/de1/a00065.html", "de/de1/a00065" ]
    ] ],
    [ "PIN.cpp", "d8/db7/a00812.html", null ],
    [ "PIN.h", "de/d9d/a00813.html", [
      [ "PIN", "d9/db3/a00066.html", "d9/db3/a00066" ]
    ] ],
    [ "PINFlux.cpp", "da/d71/a00814.html", null ],
    [ "PINFlux.h", "d8/d2d/a00815.html", [
      [ "PINFlux", "d6/d2d/a00047.html", "d6/d2d/a00047" ]
    ] ],
    [ "PINFlux2.cpp", "d7/d60/a00816.html", null ],
    [ "PINFlux2.h", "dc/d5e/a00817.html", [
      [ "PINFlux2", "d4/dca/a00048.html", "d4/dca/a00048" ]
    ] ],
    [ "Plain.cpp", "d3/d60/a00818.html", null ],
    [ "Plain.h", "d7/d4f/a00819.html", [
      [ "Plain", "db/dab/a00089.html", "db/dab/a00089" ]
    ] ],
    [ "delta_hamiltonian/PlainGC.cpp", "dc/d00/a00820.html", null ],
    [ "hamiltonian/PlainGC.cpp", "d6/d8b/a00821.html", null ],
    [ "delta_hamiltonian/PlainGC.h", "d6/da7/a00822.html", [
      [ "PlainGC", "d9/dcc/a00102.html", "d9/dcc/a00102" ]
    ] ],
    [ "hamiltonian/PlainGC.h", "d3/db4/a00823.html", [
      [ "PlainGC", "df/d98/a00107.html", "df/d98/a00107" ]
    ] ],
    [ "PlyExporter.cpp", "d8/d1c/a00824.html", null ],
    [ "PlyExporter.h", "dd/d5f/a00825.html", [
      [ "PlyExporter", "df/d24/a00217.html", "df/d24/a00217" ]
    ] ],
    [ "PlyFormat.h", "da/d90/a00826.html", [
      [ "PlyFormat", "dc/d9a/a00218.html", "dc/d9a/a00218" ]
    ] ],
    [ "PolygonUtils.cpp", "dc/d1b/a00827.html", null ],
    [ "PolygonUtils.h", "d8/d64/a00828.html", [
      [ "PolygonUtils", "d0/de8/a00133.html", "d0/de8/a00133" ]
    ] ],
    [ "Preferences.cpp", "d1/dea/a00829.html", null ],
    [ "Preferences.h", "d9/daf/a00830.html", [
      [ "Preferences", "db/d43/a00422.html", "db/d43/a00422" ]
    ] ],
    [ "PreferencesChanged.h", "d7/d5f/a00831.html", [
      [ "PreferencesChanged", "d8/d91/a00410.html", "d8/d91/a00410" ]
    ] ],
    [ "PreferencesObserver.h", "d0/d08/a00832.html", [
      [ "PreferencesObserver", "dd/df3/a00219.html", "dd/df3/a00219" ]
    ] ],
    [ "printArgv.h", "dc/d6e/a00833.html", "dc/d6e/a00833" ],
    [ "Project.cpp", "d9/d02/a00834.html", null ],
    [ "cpp_simptshell/workspace/Project.h", "dd/d70/a00835.html", "dd/d70/a00835" ],
    [ "cpp_simshell/workspace/Project.h", "d3/d13/a00836.html", [
      [ "Project", "d1/d05/a00423.html", "d1/d05/a00423" ]
    ] ],
    [ "Project_def.h", "d1/de6/a00837.html", null ],
    [ "ProjectChanged.h", "d2/dc7/a00838.html", [
      [ "ProjectChanged", "d2/db6/a00411.html", "d2/db6/a00411" ]
    ] ],
    [ "ProjectController.cpp", "df/de8/a00839.html", null ],
    [ "ProjectController.h", "d9/d61/a00840.html", [
      [ "ProjectController", "db/ddc/a00353.html", "db/ddc/a00353" ]
    ] ],
    [ "Protocol.cpp", "d5/dac/a00841.html", null ],
    [ "Protocol.h", "d0/d11/a00842.html", [
      [ "Protocol", "dd/da7/a00168.html", "dd/da7/a00168" ]
    ] ],
    [ "ptree2hdf5.cpp", "dc/d22/a00843.html", "dc/d22/a00843" ],
    [ "ptree2hdf5.h", "d3/d69/a00844.html", "d3/d69/a00844" ],
    [ "PTreeComparison.cpp", "d7/dcf/a00845.html", null ],
    [ "PTreeComparison.h", "d9/d87/a00846.html", [
      [ "PTreeComparison", "d5/d0b/a00220.html", "d5/d0b/a00220" ]
    ] ],
    [ "PTreeContainer.cpp", "d4/d60/a00847.html", null ],
    [ "PTreeContainer.h", "d9/d30/a00848.html", [
      [ "PTreeContainer", "d6/d80/a00371.html", "d6/d80/a00371" ]
    ] ],
    [ "PTreeContainerPreferencesObserver.cpp", "de/d37/a00849.html", null ],
    [ "PTreeContainerPreferencesObserver.h", "dc/d0a/a00850.html", [
      [ "PTreeContainerPreferencesObserver", "db/d7e/a00372.html", "db/d7e/a00372" ]
    ] ],
    [ "PTreeEditor.cpp", "df/d4c/a00851.html", null ],
    [ "PTreeEditor.h", "d9/dbb/a00852.html", [
      [ "PTreeEditor", "de/dd2/a00207.html", "de/dd2/a00207" ]
    ] ],
    [ "PTreeEditorWindow.cpp", "db/deb/a00853.html", null ],
    [ "PTreeEditorWindow.h", "d6/d2f/a00854.html", [
      [ "PTreeEditorWindow", "dc/de3/a00373.html", "dc/de3/a00373" ]
    ] ],
    [ "PTreeFile.cpp", "da/de6/a00855.html", null ],
    [ "PTreeFile.h", "da/de3/a00856.html", [
      [ "PTreeFile", "d2/d45/a00342.html", "d2/d45/a00342" ]
    ] ],
    [ "PTreeMenu.cpp", "d0/d82/a00857.html", null ],
    [ "PTreeMenu.h", "d0/d66/a00858.html", [
      [ "PTreeMenu", "d5/db5/a00374.html", "d5/db5/a00374" ]
    ] ],
    [ "PTreeModel.cpp", "dc/d8c/a00859.html", null ],
    [ "PTreeModel.h", "d2/d5e/a00860.html", [
      [ "PTreeModel", "dd/dcb/a00375.html", "dd/dcb/a00375" ]
    ] ],
    [ "PTreePanels.cpp", "dd/dd5/a00861.html", null ],
    [ "PTreePanels.h", "dc/d41/a00862.html", [
      [ "PTreePanels", "de/d76/a00134.html", "de/d76/a00134" ]
    ] ],
    [ "PTreeQtState.cpp", "d7/d49/a00863.html", null ],
    [ "PTreeQtState.h", "d7/d1b/a00864.html", [
      [ "PTreeQtState", "d9/da1/a00396.html", "d9/da1/a00396" ]
    ] ],
    [ "PtreeTissueBuilder.cpp", "dd/d7b/a00865.html", null ],
    [ "PtreeTissueBuilder.h", "dd/db2/a00866.html", [
      [ "PtreeTissueBuilder", "d8/dfc/a00327.html", "d8/dfc/a00327" ]
    ] ],
    [ "PTreeUndoCommands.cpp", "d2/db8/a00867.html", null ],
    [ "PTreeUndoCommands.h", "da/d5b/a00868.html", [
      [ "EditDataCommand", "d6/db8/a00376.html", "d6/db8/a00376" ],
      [ "EditKeyCommand", "d4/d13/a00377.html", "d4/d13/a00377" ],
      [ "InsertRowsCommand", "d7/d08/a00378.html", "d7/d08/a00378" ],
      [ "MoveRowsCommand", "d8/d95/a00379.html", "d8/d95/a00379" ],
      [ "RemoveRowsCommand", "d8/ddd/a00380.html", "d8/ddd/a00380" ]
    ] ],
    [ "PTreeUtils.cpp", "d3/db2/a00869.html", null ],
    [ "PTreeUtils.h", "da/d98/a00870.html", [
      [ "PTreeUtils", "d0/dc5/a00343.html", "d0/dc5/a00343" ]
    ] ],
    [ "PTreeView.cpp", "dd/de1/a00871.html", null ],
    [ "PTreeView.h", "dc/d17/a00872.html", [
      [ "PTreeView", "de/d08/a00381.html", "de/d08/a00381" ]
    ] ],
    [ "QHostAnyAddress.h", "d9/d22/a00873.html", "d9/d22/a00873" ],
    [ "QtClasses.doxy", "df/d2a/a00874.html", null ],
    [ "QtPreferences.h", "d1/d93/a00875.html", [
      [ "QtPreferences", "dd/d52/a00221.html", "dd/d52/a00221" ]
    ] ],
    [ "QtViewer.cpp", "d2/dac/a00876.html", null ],
    [ "QtViewer.h", "d1/de7/a00877.html", [
      [ "QtViewer", "d0/d24/a00222.html", "d0/d24/a00222" ]
    ] ],
    [ "RandomEngine.cpp", "d5/d69/a00878.html", "d5/d69/a00878" ],
    [ "RandomEngine.h", "d2/da1/a00879.html", [
      [ "RandomEngine", "d4/db5/a00328.html", "d4/db5/a00328" ]
    ] ],
    [ "RandomEngineType.cpp", "d1/d8f/a00880.html", "d1/d8f/a00880" ],
    [ "RandomEngineType.h", "df/d41/a00881.html", "df/d41/a00881" ],
    [ "RangeSweep.cpp", "d3/d50/a00882.html", null ],
    [ "RangeSweep.h", "d7/d34/a00883.html", [
      [ "RangeSweep", "de/d53/a00169.html", "de/d53/a00169" ]
    ] ],
    [ "RectangularTile.cpp", "dd/d08/a00884.html", null ],
    [ "RectangularTile.h", "db/d18/a00885.html", [
      [ "RectangularTile", "d5/d5b/a00135.html", "d5/d5b/a00135" ]
    ] ],
    [ "ReduceCellWalls.h", "d2/dab/a00886.html", "d2/dab/a00886" ],
    [ "RegularGeneratorDialog.cpp", "d7/d92/a00887.html", null ],
    [ "RegularGeneratorDialog.h", "dd/d94/a00888.html", [
      [ "RegularGeneratorDialog", "d0/d5e/a00136.html", "d0/d5e/a00136" ]
    ] ],
    [ "RegularTiling.cpp", "de/dc8/a00889.html", null ],
    [ "RegularTiling.h", "d7/d46/a00890.html", [
      [ "RegularTiling", "d2/df5/a00137.html", "d2/df5/a00137" ]
    ] ],
    [ "RevisionInfo.h", "de/dfc/a00891.html", [
      [ "RevisionInfo", "d8/d8e/a00344.html", "d8/d8e/a00344" ]
    ] ],
    [ "Rgb2Hsv.cpp", "d0/de2/a00892.html", "d0/de2/a00892" ],
    [ "Rgb2Hsv.h", "dc/d2c/a00893.html", "dc/d2c/a00893" ],
    [ "RootViewerNode.cpp", "dc/dfa/a00894.html", null ],
    [ "RootViewerNode.h", "d2/d21/a00895.html", [
      [ "RootViewerNode", "d3/d42/a00235.html", "d3/d42/a00235" ]
    ] ],
    [ "SaveChangesDialog.cpp", "da/dd6/a00896.html", null ],
    [ "SaveChangesDialog.h", "de/d01/a00897.html", [
      [ "SaveChangesDialog", "d3/d45/a00382.html", "d3/d45/a00382" ]
    ] ],
    [ "SegmentedVector.h", "d2/d79/a00898.html", [
      [ "SegmentedVector", "de/d8c/a00036.html", "de/d8c/a00036" ]
    ] ],
    [ "SelectByIDWidget.cpp", "d3/d8a/a00899.html", null ],
    [ "SelectByIDWidget.h", "d9/db0/a00900.html", [
      [ "SelectByIDWidget", "d4/ddd/a00138.html", "d4/ddd/a00138" ]
    ] ],
    [ "SendPage.cpp", "d0/dac/a00901.html", null ],
    [ "SendPage.h", "d5/dd3/a00902.html", [
      [ "SendPage", "d5/dcb/a00170.html", "d5/dcb/a00170" ]
    ] ],
    [ "Server.cpp", "d4/d1c/a00903.html", null ],
    [ "Server.h", "d8/d0a/a00904.html", [
      [ "Server", "d1/d0d/a00171.html", "d1/d0d/a00171" ]
    ] ],
    [ "ServerClientProtocol.cpp", "da/d5b/a00905.html", null ],
    [ "ServerClientProtocol.h", "da/dd8/a00906.html", [
      [ "ServerClientProtocol", "d6/dfb/a00172.html", "d6/dfb/a00172" ]
    ] ],
    [ "ServerDialog.cpp", "d5/d89/a00907.html", null ],
    [ "ServerDialog.h", "d0/d85/a00908.html", [
      [ "ServerDialog", "d7/d3f/a00173.html", "d7/d3f/a00173" ]
    ] ],
    [ "ServerInfo.cpp", "de/dc3/a00909.html", null ],
    [ "ServerInfo.h", "d4/d12/a00910.html", [
      [ "ServerInfo", "d8/df1/a00174.html", "d8/df1/a00174" ]
    ] ],
    [ "ServerNodeProtocol.cpp", "d7/d12/a00911.html", null ],
    [ "ServerNodeProtocol.h", "df/da5/a00912.html", [
      [ "ServerNodeProtocol", "de/d45/a00175.html", "de/d45/a00175" ]
    ] ],
    [ "SessionController.cpp", "d9/dfb/a00913.html", null ],
    [ "SessionController.h", "d3/dd1/a00914.html", [
      [ "SessionController", "d0/dc3/a00354.html", "d0/dc3/a00354" ]
    ] ],
    [ "signum.h", "d2/d5e/a00915.html", "d2/d5e/a00915" ],
    [ "Sim.cpp", "d6/d66/a00916.html", null ],
    [ "Sim.h", "d4/ddc/a00917.html", [
      [ "Sim", "dd/dbb/a00330.html", "dd/dbb/a00330" ]
    ] ],
    [ "sim_cli.cpp", "d0/ddf/a00918.html", null ],
    [ "sim_cli_mode.h", "d1/d0b/a00919.html", [
      [ "simPTCLIMode", "df/d86/a00012.html", "df/d86/a00012" ]
    ] ],
    [ "sim_gui.cpp", "d6/d82/a00920.html", null ],
    [ "sim_gui_mode.h", "dd/d7a/a00921.html", [
      [ "simPTGUIMode", "d7/dd4/a00013.html", "d7/dd4/a00013" ]
    ] ],
    [ "SimEvent.h", "dd/dcf/a00922.html", [
      [ "SimEvent", "da/d50/a00301.html", "da/d50/a00301" ]
    ] ],
    [ "SimEventType.h", "df/de9/a00923.html", "df/de9/a00923" ],
    [ "SimInterface.h", "de/d62/a00924.html", [
      [ "SimInterface", "d8/df7/a00331.html", "d8/df7/a00331" ]
    ] ],
    [ "SimPhase.h", "dc/d36/a00925.html", "dc/d36/a00925" ],
    [ "SimplyRed.cpp", "d8/d38/a00926.html", null ],
    [ "SimplyRed.h", "dd/d98/a00927.html", [
      [ "SimplyRed", "d7/df7/a00058.html", "d7/df7/a00058" ]
    ] ],
    [ "simPT_editor.cpp", "dd/d38/a00928.html", "dd/d38/a00928" ],
    [ "simPT_parex.cpp", "dc/d92/a00929.html", "dc/d92/a00929" ],
    [ "simPT_sim.cpp", "d7/d16/a00930.html", "d7/d16/a00930" ],
    [ "SimResult.cpp", "df/dbc/a00931.html", null ],
    [ "SimResult.h", "d8/dbb/a00932.html", [
      [ "SimResult", "d5/de9/a00176.html", "d5/de9/a00176" ]
    ] ],
    [ "SimSession.cpp", "d0/d5b/a00933.html", null ],
    [ "SimSession.h", "d0/df0/a00934.html", [
      [ "SimSession", "d5/ddf/a00224.html", "d5/ddf/a00224" ]
    ] ],
    [ "SimSessionCoupled.cpp", "d8/d19/a00935.html", null ],
    [ "SimSessionCoupled.h", "dc/d2b/a00936.html", [
      [ "SimSessionCoupled", "dc/d98/a00225.html", "dc/d98/a00225" ]
    ] ],
    [ "SimState.cpp", "de/db9/a00937.html", null ],
    [ "SimState.h", "d5/d54/a00938.html", [
      [ "SimState", "dc/d50/a00332.html", "dc/d50/a00332" ]
    ] ],
    [ "SimTask.cpp", "dd/d20/a00939.html", null ],
    [ "SimTask.h", "d7/db6/a00940.html", [
      [ "SimTask", "dd/d4c/a00177.html", "dd/d4c/a00177" ]
    ] ],
    [ "SimTimingTraits.h", "d6/dad/a00941.html", "d6/dad/a00941" ],
    [ "Simulator.cpp", "df/d12/a00942.html", "df/d12/a00942" ],
    [ "Simulator.h", "d1/ddd/a00943.html", [
      [ "Simulator", "d9/d90/a00178.html", "d9/d90/a00178" ]
    ] ],
    [ "SimWorker.cpp", "d9/dce/a00944.html", null ],
    [ "SimWorker.h", "d1/db0/a00945.html", [
      [ "SimWorker", "dc/d55/a00226.html", "dc/d55/a00226" ]
    ] ],
    [ "SimWrapper.cpp", "da/d40/a00946.html", null ],
    [ "SimWrapper.h", "d3/d29/a00947.html", "d3/d29/a00947" ],
    [ "Size.cpp", "d8/d18/a00948.html", null ],
    [ "Size.h", "df/dc6/a00949.html", [
      [ "Size", "d3/d8b/a00059.html", "d3/d8b/a00059" ]
    ] ],
    [ "SliceItem.cpp", "df/d39/a00950.html", null ],
    [ "SliceItem.h", "dc/da0/a00951.html", [
      [ "SliceItem", "de/d14/a00139.html", "de/d14/a00139" ]
    ] ],
    [ "cell2cell_transport/SmithPhyllotaxis.cpp", "de/d49/a00952.html", null ],
    [ "cell_chemistry/SmithPhyllotaxis.cpp", "d5/da3/a00953.html", null ],
    [ "cell_daughters/SmithPhyllotaxis.cpp", "d3/dca/a00954.html", null ],
    [ "cell_housekeep/SmithPhyllotaxis.cpp", "dc/db6/a00955.html", null ],
    [ "cell2cell_transport/SmithPhyllotaxis.h", "d3/de5/a00956.html", [
      [ "SmithPhyllotaxis", "d9/dec/a00090.html", "d9/dec/a00090" ]
    ] ],
    [ "cell_chemistry/SmithPhyllotaxis.h", "d1/df7/a00957.html", [
      [ "SmithPhyllotaxis", "d2/dee/a00049.html", "d2/dee/a00049" ]
    ] ],
    [ "cell_daughters/SmithPhyllotaxis.h", "df/d9c/a00958.html", [
      [ "SmithPhyllotaxis", "dc/ddf/a00067.html", "dc/ddf/a00067" ]
    ] ],
    [ "cell_housekeep/SmithPhyllotaxis.h", "db/d40/a00959.html", [
      [ "SmithPhyllotaxis", "db/df8/a00076.html", "db/df8/a00076" ]
    ] ],
    [ "cell2cell_transport/Source.cpp", "dd/d11/a00960.html", null ],
    [ "cell_chemistry/Source.cpp", "da/d45/a00961.html", null ],
    [ "cell2cell_transport/Source.h", "df/d88/a00962.html", [
      [ "Source", "da/d82/a00091.html", "da/d82/a00091" ]
    ] ],
    [ "cell_chemistry/Source.h", "de/dad/a00963.html", [
      [ "Source", "d5/de0/a00050.html", "d5/de0/a00050" ]
    ] ],
    [ "StandardNormal.h", "da/d17/a00964.html", [
      [ "StandardNormal", "d4/d81/a00110.html", "d4/d81/a00110" ]
    ] ],
    [ "StandardUniform.h", "d3/dcd/a00965.html", [
      [ "StandardUniform", "dd/d6d/a00111.html", "dd/d6d/a00111" ]
    ] ],
    [ "StartPage.cpp", "d2/d6e/a00966.html", null ],
    [ "StartPage.h", "d4/d94/a00967.html", [
      [ "StartPage", "db/dd9/a00179.html", "db/dd9/a00179" ]
    ] ],
    [ "StartupFileBase.cpp", "d7/dd4/a00968.html", null ],
    [ "StartupFileBase.h", "d7/dfd/a00969.html", [
      [ "StartupFileBase", "d9/d21/a00240.html", "d9/d21/a00240" ]
    ] ],
    [ "StartupFileHdf5.cpp", "d0/daf/a00970.html", null ],
    [ "StartupFileHdf5.h", "d0/d3c/a00971.html", [
      [ "StartupFileHdf5", "d6/db6/a00241.html", "d6/db6/a00241" ]
    ] ],
    [ "StartupFilePtree.cpp", "de/d4e/a00972.html", null ],
    [ "StartupFilePtree.h", "d3/da5/a00973.html", [
      [ "StartupFilePtree", "d2/dc8/a00242.html", "d2/dc8/a00242" ]
    ] ],
    [ "StartupFileXml.cpp", "df/dce/a00974.html", null ],
    [ "StartupFileXml.h", "db/dbf/a00975.html", [
      [ "StartupFileXml", "d6/d82/a00243.html", "d6/d82/a00243" ]
    ] ],
    [ "StartupFileXmlGz.cpp", "d0/d85/a00976.html", null ],
    [ "StartupFileXmlGz.h", "d5/d6c/a00977.html", [
      [ "StartupFileXmlGz", "d6/d08/a00244.html", "d6/d08/a00244" ]
    ] ],
    [ "Status.cpp", "d2/ddd/a00978.html", null ],
    [ "Status.h", "d8/d49/a00979.html", [
      [ "Status", "d0/d81/a00180.html", "d0/d81/a00180" ]
    ] ],
    [ "stdClasses.doxy", "db/d20/a00980.html", null ],
    [ "StepFilterProxyModel.cpp", "dd/d24/a00981.html", null ],
    [ "StepFilterProxyModel.h", "d1/d7a/a00982.html", [
      [ "StepFilterProxyModel", "d9/d01/a00227.html", "d9/d01/a00227" ]
    ] ],
    [ "StepSelection.cpp", "d7/d98/a00983.html", null ],
    [ "StepSelection.h", "d2/d4c/a00984.html", [
      [ "StepSelection", "de/dc2/a00228.html", "de/dc2/a00228" ]
    ] ],
    [ "Stopwatch.h", "da/deb/a00985.html", "da/deb/a00985" ],
    [ "StringUtils.h", "d3/d7b/a00986.html", "d3/d7b/a00986" ],
    [ "Subject.h", "d4/d21/a00987.html", [
      [ "Subject", "d9/d48/a00345.html", "d9/d48/a00345" ],
      [ "Subject< E, std::weak_ptr< const void > >", "d2/daf/a00346.html", "d2/daf/a00346" ]
    ] ],
    [ "SubjectNode.h", "d6/d48/a00988.html", [
      [ "SubjectNode", "d9/de6/a00404.html", "d9/de6/a00404" ],
      [ "SubjectViewerNodeWrapper", "de/de8/a00405.html", "de/de8/a00405" ]
    ] ],
    [ "SVIterator.h", "d4/d95/a00989.html", [
      [ "SegmentedVector", "de/d8c/a00036.html", "de/d8c/a00036" ],
      [ "SVIterator", "d8/d0a/a00292.html", "d8/d0a/a00292" ]
    ] ],
    [ "TaskOverview.cpp", "dd/d7a/a00990.html", null ],
    [ "TaskOverview.h", "df/d2f/a00991.html", [
      [ "TaskOverview", "d8/d64/a00181.html", "d8/d64/a00181" ]
    ] ],
    [ "TemplateFilePage.cpp", "d7/d76/a00992.html", "d7/d76/a00992" ],
    [ "TemplateFilePage.h", "da/d0a/a00993.html", [
      [ "TemplateFilePage", "d7/d25/a00182.html", "d7/d25/a00182" ]
    ] ],
    [ "cell2cell_transport/TestCoupling.cpp", "d4/dd0/a00994.html", null ],
    [ "cell_chemistry/TestCoupling.cpp", "dd/d48/a00995.html", null ],
    [ "cell2cell_transport/TestCoupling.h", "d3/daa/a00996.html", [
      [ "TestCoupling", "d1/dc1/a00092.html", "d1/dc1/a00092" ]
    ] ],
    [ "cell_chemistry/TestCoupling.h", "d0/d28/a00997.html", [
      [ "TestCoupling", "d2/d79/a00051.html", "d2/d79/a00051" ]
    ] ],
    [ "TestCoupling_I.cpp", "d9/d9a/a00998.html", null ],
    [ "TestCoupling_I.h", "de/de2/a00999.html", [
      [ "TestCoupling_I", "d9/d77/a00095.html", "d9/d77/a00095" ]
    ] ],
    [ "TestCoupling_II.cpp", "d7/df2/a01000.html", null ],
    [ "TestCoupling_II.h", "df/d59/a01001.html", [
      [ "TestCoupling_II", "d9/df8/a00096.html", "d9/df8/a00096" ]
    ] ],
    [ "TextViewer.h", "d2/dd5/a01002.html", [
      [ "TextViewer", "dd/d2d/a00229.html", "dd/d2d/a00229" ]
    ] ],
    [ "TextViewerPreferences.h", "dc/d10/a01003.html", [
      [ "TextViewerPreferences", "d1/da8/a00230.html", "d1/da8/a00230" ]
    ] ],
    [ "Tile.h", "df/dbc/a01004.html", [
      [ "Tile", "d5/db1/a00140.html", "d5/db1/a00140" ]
    ] ],
    [ "Timeable.h", "d7/d06/a01005.html", [
      [ "Timeable", "d0/d19/a00273.html", "d0/d19/a00273" ]
    ] ],
    [ "TimeData.h", "dc/d81/a01006.html", [
      [ "TimeData", "dd/d1d/a00336.html", "dd/d1d/a00336" ]
    ] ],
    [ "TimeSlicer.cpp", "d6/d8d/a01007.html", null ],
    [ "TimeSlicer.h", "dd/dac/a01008.html", [
      [ "TimeSlicer", "dc/d84/a00338.html", "dc/d84/a00338" ]
    ] ],
    [ "TimeStamp.h", "d7/ded/a01009.html", "d7/ded/a01009" ],
    [ "TimeStepPostfixFormat.h", "d0/d2b/a01010.html", [
      [ "TimeStepPostfixFormat", "df/d17/a00231.html", "df/d17/a00231" ]
    ] ],
    [ "TipGrowth.cpp", "d6/d36/a01011.html", null ],
    [ "TipGrowth.h", "d4/d45/a01012.html", [
      [ "TipGrowth", "d9/d73/a00060.html", "d9/d73/a00060" ]
    ] ],
    [ "Tissue.h", "d7/d59/a01013.html", [
      [ "Tissue", "d0/d01/a00339.html", "d0/d01/a00339" ]
    ] ],
    [ "TissueEditor.cpp", "d0/d56/a01014.html", null ],
    [ "TissueEditor.h", "d1/dac/a01015.html", [
      [ "TissueEditor", "d5/d4e/a00141.html", "d5/d4e/a00141" ]
    ] ],
    [ "TissueGraphicsView.cpp", "de/def/a01016.html", null ],
    [ "TissueGraphicsView.h", "d3/ded/a01017.html", "d3/ded/a01017" ],
    [ "TissueSlicer.cpp", "d3/d08/a01018.html", null ],
    [ "TissueSlicer.h", "db/daf/a01019.html", [
      [ "TissueSlicer", "d7/dce/a00143.html", "d7/dce/a00143" ]
    ] ],
    [ "TransformationWidget.cpp", "d0/d43/a01020.html", null ],
    [ "TransformationWidget.h", "d5/dfb/a01021.html", [
      [ "TransformationWidget", "d5/da7/a00144.html", "d5/da7/a00144" ]
    ] ],
    [ "Transport.cpp", "d1/dd8/a01022.html", null ],
    [ "Transport.h", "d6/dcd/a01023.html", [
      [ "Transport", "d2/da3/a00115.html", "d2/da3/a00115" ]
    ] ],
    [ "TransportEquations.cpp", "d4/dc5/a01024.html", null ],
    [ "TransportEquations.h", "db/d23/a01025.html", [
      [ "TransportEquations", "df/df1/a00340.html", "df/df1/a00340" ]
    ] ],
    [ "TriangularTile.cpp", "de/d02/a01026.html", null ],
    [ "TriangularTile.h", "dd/dfd/a01027.html", [
      [ "TriangularTile", "da/ddb/a00145.html", "da/ddb/a00145" ]
    ] ],
    [ "UndoStack.cpp", "db/d08/a01028.html", null ],
    [ "UndoStack.h", "d0/daa/a01029.html", [
      [ "UndoStack", "d9/db6/a00146.html", "d9/db6/a00146" ]
    ] ],
    [ "Utils.h", "de/d41/a01030.html", [
      [ "Utils", "d3/d5e/a00275.html", "d3/d5e/a00275" ]
    ] ],
    [ "VectorFormat.cpp", "d6/dcf/a01031.html", null ],
    [ "VectorFormat.h", "df/d89/a01032.html", [
      [ "VectorFormat", "d0/d53/a00232.html", "d0/d53/a00232" ]
    ] ],
    [ "VectorGraphicsExporter.cpp", "d2/d57/a01033.html", null ],
    [ "VectorGraphicsExporter.h", "de/d91/a01034.html", [
      [ "VectorGraphicsExporter", "d6/de1/a00233.html", "d6/de1/a00233" ]
    ] ],
    [ "VectorGraphicsPreferences.h", "de/d26/a01035.html", [
      [ "VectorGraphicsPreferences", "da/d31/a00234.html", "da/d31/a00234" ]
    ] ],
    [ "ViewerActions.cpp", "d8/d79/a01036.html", null ],
    [ "ViewerActions.h", "d2/d41/a01037.html", [
      [ "ViewerActions", "d3/d30/a00370.html", "d3/d30/a00370" ]
    ] ],
    [ "ViewerDockWidget.cpp", "d4/dbc/a01038.html", null ],
    [ "ViewerDockWidget.h", "df/d21/a01039.html", [
      [ "ViewerDockWidget", "da/dee/a00383.html", "da/dee/a00383" ]
    ] ],
    [ "ViewerEvent.h", "db/d14/a01040.html", [
      [ "ViewerEvent", "d1/d53/a00399.html", "d1/d53/a00399" ]
    ] ],
    [ "ViewerNode.h", "d1/d83/a01041.html", [
      [ "InitNotifier", "d9/d95/a00400.html", "d9/d95/a00400" ],
      [ "Register", "d4/d6d/a00402.html", "d4/d6d/a00402" ],
      [ "Register< true >", "d0/da0/a00403.html", "d0/da0/a00403" ],
      [ "viewer_is_subject", "d1/d8b/a00406.html", "d1/d8b/a00406" ],
      [ "viewer_is_widget", "d1/d98/a00407.html", "d1/d98/a00407" ],
      [ "ViewerNode", "dd/dea/a00408.html", "dd/dea/a00408" ]
    ] ],
    [ "ViewerWindow.cpp", "d4/d56/a01042.html", null ],
    [ "ViewerWindow.h", "d8/d27/a01043.html", [
      [ "ViewerWindow", "d2/d56/a00384.html", "d2/d56/a00384" ]
    ] ],
    [ "VLeaf.cpp", "d0/ddd/a01044.html", null ],
    [ "VLeaf.h", "d7/d46/a01045.html", [
      [ "VLeaf", "d4/d3e/a00116.html", "d4/d3e/a00116" ]
    ] ],
    [ "VoronoiGeneratorDialog.cpp", "da/d17/a01046.html", null ],
    [ "VoronoiGeneratorDialog.h", "db/dbb/a01047.html", [
      [ "VoronoiGeneratorDialog", "d0/d4a/a00147.html", "d0/d4a/a00147" ]
    ] ],
    [ "VoronoiTesselation.cpp", "d5/de7/a01048.html", null ],
    [ "VoronoiTesselation.h", "d0/d22/a01049.html", [
      [ "VoronoiTesselation", "db/d48/a00148.html", "db/d48/a00148" ]
    ] ],
    [ "VPTissue.cpp", "d9/d8e/a01050.html", null ],
    [ "VPTissue.h", "d3/d42/a01051.html", [
      [ "VPTissue", "d9/d4b/a00117.html", "d9/d4b/a00117" ]
    ] ],
    [ "Wall.cpp", "d9/dce/a01052.html", "d9/dce/a01052" ],
    [ "Wall.h", "dc/d99/a01053.html", "dc/d99/a01053" ],
    [ "WallAttributes.cpp", "dd/d57/a01054.html", null ],
    [ "WallAttributes.h", "d2/d71/a01055.html", [
      [ "WallAttributes", "d7/dfc/a00349.html", "d7/dfc/a00349" ]
    ] ],
    [ "WallItem.cpp", "dc/d19/a01056.html", null ],
    [ "WallItem.h", "d2/db8/a01057.html", [
      [ "WallItem", "d7/d40/a00236.html", "d7/d40/a00236" ]
    ] ],
    [ "WallType.cpp", "d5/d4c/a01058.html", "d5/d4c/a01058" ],
    [ "WallType.h", "d0/d90/a01059.html", "d0/d90/a01059" ],
    [ "WorkerNode.cpp", "d5/d16/a01060.html", null ],
    [ "WorkerNode.h", "de/d9e/a01061.html", [
      [ "WorkerNode", "dc/d52/a00183.html", "dc/d52/a00183" ]
    ] ],
    [ "WorkerPool.cpp", "d4/d8a/a01062.html", "d4/d8a/a01062" ],
    [ "WorkerPool.h", "de/de6/a01063.html", [
      [ "WorkerPool", "d2/d94/a00184.html", "d2/d94/a00184" ]
    ] ],
    [ "WorkerRepresentative.cpp", "d9/db3/a01064.html", null ],
    [ "WorkerRepresentative.h", "d0/dc9/a01065.html", [
      [ "WorkerRepresentative", "de/df3/a00185.html", "de/df3/a00185" ]
    ] ],
    [ "Workspace.cpp", "d0/dfb/a01066.html", null ],
    [ "cpp_simptshell/workspace/Workspace.h", "d2/d49/a01067.html", [
      [ "Workspace", "d4/d9a/a00246.html", "d4/d9a/a00246" ]
    ] ],
    [ "cpp_simshell/workspace/Workspace.h", "d1/de6/a01068.html", [
      [ "Workspace", "dc/d2f/a00425.html", "dc/d2f/a00425" ]
    ] ],
    [ "Workspace_def.h", "d8/d18/a01069.html", null ],
    [ "WorkspaceChanged.h", "d5/d67/a01070.html", [
      [ "WorkspaceChanged", "d9/d72/a00412.html", "d9/d72/a00412" ]
    ] ],
    [ "WorkspaceController.cpp", "df/d18/a01071.html", null ],
    [ "WorkspaceController.h", "d6/d6a/a01072.html", [
      [ "WorkspaceController", "d3/dc9/a00355.html", "d3/dc9/a00355" ]
    ] ],
    [ "WorkspaceFactory.cpp", "d9/d7c/a01073.html", null ],
    [ "WorkspaceFactory.h", "d7/df7/a01074.html", [
      [ "WorkspaceFactory", "d0/d1f/a00247.html", "d0/d1f/a00247" ]
    ] ],
    [ "WorkspaceQtModel.cpp", "dd/d80/a01075.html", null ],
    [ "WorkspaceQtModel.h", "d6/d80/a01076.html", [
      [ "WorkspaceQtModel", "d5/d05/a00385.html", "d5/d05/a00385" ],
      [ "FI", "dd/df5/a00386.html", "dd/df5/a00386" ],
      [ "PR", "db/db9/a00387.html", "db/db9/a00387" ],
      [ "WS", "d9/dc1/a00388.html", "d9/dc1/a00388" ]
    ] ],
    [ "WorkspaceView.cpp", "d0/dd3/a01077.html", null ],
    [ "WorkspaceView.h", "db/dba/a01078.html", [
      [ "WorkspaceView", "dc/dc9/a00389.html", "dc/dc9/a00389" ]
    ] ],
    [ "WorkspaceWizard.cpp", "d2/d4d/a01079.html", null ],
    [ "WorkspaceWizard.h", "d9/d67/a01080.html", [
      [ "WorkspaceWizard", "d9/d0a/a00390.html", "d9/d0a/a00390" ],
      [ "DonePage", "db/d95/a00391.html", "db/d95/a00391" ],
      [ "ExistingPage", "d4/d27/a00392.html", "d4/d27/a00392" ],
      [ "InitPage", "da/dbf/a00393.html", "da/dbf/a00393" ],
      [ "PathPage", "d9/dad/a00394.html", "d9/dad/a00394" ]
    ] ],
    [ "cell2cell_transport/Wortel.cpp", "dd/dee/a01081.html", null ],
    [ "cell_chemistry/Wortel.cpp", "dd/df1/a01082.html", null ],
    [ "cell_color/Wortel.cpp", "d6/d9a/a01083.html", null ],
    [ "cell_daughters/Wortel.cpp", "da/d93/a01084.html", null ],
    [ "cell_housekeep/Wortel.cpp", "d2/df3/a01085.html", null ],
    [ "cell_split/Wortel.cpp", "d9/df5/a01086.html", null ],
    [ "cell2cell_transport/Wortel.h", "d0/d1f/a01087.html", [
      [ "Wortel", "d4/d7d/a00093.html", "d4/d7d/a00093" ]
    ] ],
    [ "cell_chemistry/Wortel.h", "da/db7/a01088.html", [
      [ "Wortel", "d1/d84/a00052.html", "d1/d84/a00052" ]
    ] ],
    [ "cell_color/Wortel.h", "d3/dc4/a01089.html", [
      [ "Wortel", "d4/de8/a00061.html", "d4/de8/a00061" ]
    ] ],
    [ "cell_daughters/Wortel.h", "da/d73/a01090.html", [
      [ "Wortel", "da/d24/a00068.html", "da/d24/a00068" ]
    ] ],
    [ "cell_housekeep/Wortel.h", "d6/d0d/a01091.html", [
      [ "Wortel", "d1/d09/a00077.html", "d1/d09/a00077" ]
    ] ],
    [ "cell_split/Wortel.h", "de/d9c/a01092.html", [
      [ "Wortel", "d2/d1f/a00083.html", "d2/d1f/a00083" ]
    ] ],
    [ "cell2cell_transport/WrapperModel.cpp", "de/d1c/a01093.html", null ],
    [ "cell_chemistry/WrapperModel.cpp", "d6/dea/a01094.html", null ],
    [ "cell_daughters/WrapperModel.cpp", "de/d6d/a01095.html", null ],
    [ "cell_housekeep/WrapperModel.cpp", "d2/d20/a01096.html", null ],
    [ "cell_split/WrapperModel.cpp", "d1/dfa/a01097.html", null ],
    [ "cell2cell_transport/WrapperModel.h", "d5/d17/a01098.html", [
      [ "WrapperModel", "dc/d54/a00094.html", "dc/d54/a00094" ]
    ] ],
    [ "cell_chemistry/WrapperModel.h", "d6/dbc/a01099.html", [
      [ "WrapperModel", "dc/dee/a00053.html", "dc/dee/a00053" ]
    ] ],
    [ "cell_daughters/WrapperModel.h", "d3/dbd/a01100.html", [
      [ "WrapperModel", "d4/d31/a00069.html", "d4/d31/a00069" ]
    ] ],
    [ "cell_housekeep/WrapperModel.h", "de/d99/a01101.html", [
      [ "WrapperModel", "d9/da4/a00078.html", "d9/da4/a00078" ]
    ] ],
    [ "cell_split/WrapperModel.h", "d4/da7/a01102.html", [
      [ "WrapperModel", "df/d5d/a00084.html", "df/d5d/a00084" ]
    ] ],
    [ "XmlExporter.cpp", "d9/d10/a01103.html", null ],
    [ "XmlExporter.h", "dd/dcb/a01104.html", [
      [ "XmlExporter", "dd/dd7/a00248.html", "dd/dd7/a00248" ]
    ] ],
    [ "XmlFormat.h", "de/d1d/a01105.html", [
      [ "XmlFormat", "de/dbc/a00249.html", "de/dbc/a00249" ]
    ] ],
    [ "XmlGzExporter.cpp", "da/d4e/a01106.html", null ],
    [ "XmlGzExporter.h", "de/d4a/a01107.html", [
      [ "XmlGzExporter", "d0/da7/a00250.html", "d0/da7/a00250" ]
    ] ],
    [ "XmlGzFormat.h", "d2/dd7/a01108.html", [
      [ "XmlGzFormat", "d3/d1a/a00251.html", "d3/d1a/a00251" ]
    ] ],
    [ "XmlViewer.cpp", "d8/dbf/a01109.html", null ],
    [ "XmlViewer.h", "d1/d3d/a01110.html", "d1/d3d/a01110" ],
    [ "XmlWriterSettings.h", "d3/de1/a01111.html", [
      [ "XmlWriterSettings", "d1/dc6/a00347.html", "d1/dc6/a00347" ]
    ] ]
];