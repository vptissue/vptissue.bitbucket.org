var a00189 =
[
    [ "Format", "da/deb/a00189.html#a090158ab2de742af780107a7005d637a", [
      [ "Png", "da/deb/a00189.html#a090158ab2de742af780107a7005d637aa3e04b70d12f46ad261c4a4a1ffb03f01", null ],
      [ "Bmp", "da/deb/a00189.html#a090158ab2de742af780107a7005d637aa3226deac206abc829c96240f9e89dc52", null ],
      [ "Jpeg", "da/deb/a00189.html#a090158ab2de742af780107a7005d637aab2e51183fbd2f3b92ca383ff8bca66b3", null ]
    ] ],
    [ "BitmapGraphicsPreferences", "da/deb/a00189.html#ab502150b8be42495c93b7c2dc4345a03", null ],
    [ "Update", "da/deb/a00189.html#ad71f0520485c780885f719357cd6ddc6", null ],
    [ "m_format", "da/deb/a00189.html#a782c5ba77e6d17b20a7a68789d15dfc4", null ],
    [ "m_size_preset", "da/deb/a00189.html#a7080cd4cddf6a34d4be63bf8b6010f2f", null ],
    [ "m_size_x", "da/deb/a00189.html#a3108d77c8b71ce387a6a143d32cedf8b", null ],
    [ "m_size_y", "da/deb/a00189.html#ac53a1e64a6fb3e1e47a0240287fa486c", null ]
];